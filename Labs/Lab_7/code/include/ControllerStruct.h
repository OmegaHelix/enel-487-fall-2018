#ifndef __CONTROLLER_STRUCT
#define __CONTROLLER_STRUCT


#include <stdint.h>


/*
this is a simple data struct to allow the 
ADC_Interrupt and CLI to share an RTOS Queue.

size of struct is 0x24 bytes


*/



struct ControllerData {
	
	enum {ADC, CLI, BAD} source;
	
	union {
		uint32_t percent;
	
		uint32_t adc_value;
	};
	
};

#endif

