#include "USART2_Driver.h"



xQueueHandle USART2_Driver::xCLIInQueue = NULL;

const uint32_t USART2_Driver::_USART2 = 0x40004400;

const uint32_t USART2_Driver::RXNE = (0x1 << 5);

const uint8_t USART2_Driver::ENTER = 0xD;
const uint8_t USART2_Driver::BKSP = 0x7F;

bool volatile USART2_Driver::ready_buffer = false;
bool volatile USART2_Driver::clear_buffer = false;
bool volatile USART2_Driver::overflow = false;

const uint32_t USART2_Driver::RX_SIZE = 31;

bool USART2_Driver::height = false;
uint32_t USART2_Driver::percent = 0;

//USART2 Registers
USART USART2_Driver::USART2(_USART2);


//Character buffer for the USART buffers
uint8_t  USART2_Driver::RXBUFFER[USART2_Driver::RX_SIZE] = {0};

//indeces for buffer positions
 int32_t USART2_Driver::rx_in_index = 0;
 int32_t USART2_Driver::rx_out_index = 0;

USART2_Driver::USART2_Driver(){}
USART2_Driver::~USART2_Driver(){}


// sendByte is still in polling mode
void USART2_Driver::SendByte(uint8_t byte)
{
	while(!(USART2.Read(USART::SR) & (0x1 << 7)));
	
	USART2.Write(USART::DR, byte);


} 

//GetByte is in interrupt mode
uint8_t USART2_Driver::GetByte()
{
	// if the input buffer isn't empty
	if(rx_in_index - rx_out_index != 0)
	{
		//grab the character from the buffer
		return RXBUFFER[(rx_out_index++) & (RX_SIZE-1)];
	}
	return 0;
}

void USART2_Driver::open()
{
	// 1. Enable RCC GPIOA
	RCC::Or(RCC::APB2ENR, RCC::_APB2ENR_IOPAEN);
	RCC::Or(RCC::APB2ENR, RCC::_APB2ENR_AFIOEN);

	// 2. Configure Tx and Rx GPIO pins
	//Rx Pin
	//floating input 01 00
	//Clear GPIOA port 3 configuration
	GPIO::And(GPIO::A, GPIO_PORT::CRL, ~(0xF << 12));
	//Set GPIOA port 3 configuration
	GPIO::Or(GPIO::A, GPIO_PORT::CRL, 0x4 << 12);
	
	//Tx Pin
	//AF out Push pull 10 11
	//Clear GPIOA port 2 configuration
	GPIO::And(GPIO::A, GPIO_PORT::CRL, ~(0xF << 8));
	//Set GPIOA port 2 configuration
	GPIO::Or(GPIO::A, GPIO_PORT::CRL, 0xB << 8);
	
	//3. Enable USART2 Clock
	RCC::Or(RCC::APB1ENR, RCC::_APB1ENR_USART2EN);
	
	//4. Set Baud Rate to 9600 0xEA6
	//USARTDIV = f(ck)/baud
	//USARTDIV = 36M/9600 = 0xEA6
	USART2.Write(USART::BRR, 0xEA6);
	
	//5. Enable Tx/Rx
	// enable Tx
	USART2.Set(USART::CR1, Register::b3);
	
	// enable Rx
	USART2.Set(USART::CR1, Register::b2);
	
	// reset value already has default configuration of 
	// 8 bits data, no parity, and 1 stop bit.

	// enable Recieve interrupts
        USART2.Set(USART::CR1, Register::b5);
        // enable interrupt in NVIC ISER
	// NVIC ISER base address is 0xE000E100
	// there is 3 registers in NVIC ISER (0,1,2)
	// each register is at a 4 byte offset
	// USART2 is interrupt #38
        Register NVIC_ISER1(0xE000E104);
        NVIC_ISER1.Set(Register::b6);
	
}

void USART2_Driver::close()
{
	// undo whatever open did
	// 1. Disable RCC GPIOA
	RCC::And(RCC::APB2ENR, ~RCC::_APB2ENR_IOPAEN);

	// 2. Configure Tx and Rx GPIO pins
	//Rx Pin
	//AF out Push pull 10 11
	//Clear GPIOA port 3 configuration
	GPIO::And(GPIO::A, GPIO_PORT::CRL, ~(0xF << 12));
	//Set GPIOA port 3 configuration
	GPIO::And(GPIO::A, GPIO_PORT::CRL, ~(0x4 << 12));
	
	//Tx Pin
	//floating input 01 00
	//Clear GPIOA port 2 configuration
	GPIO::And(GPIO::A, GPIO_PORT::CRL, ~(0xF << 8));
	//Set GPIOA port 2 configuration
	GPIO::And(GPIO::A, GPIO_PORT::CRL, ~(0xB << 8));
	
	//3. Disable USART2 Clock
	RCC::And(RCC::APB1ENR, ~RCC::_APB1ENR_USART2EN);
	
	//4. Undo set Baud Rate to 9600 0xEA6
	//USARTDIV = f(ck)/baud
	//USARTDIV = 36M/9600 = 0xEA6
	USART2.Write(USART::BRR, ~0xEA6);
	
	//5. Disable Tx/Rx
	// Disable Tx
	USART2.Reset(USART::CR1, Register::b3);
	
	// Disable Rx
	USART2.Reset(USART::CR1, Register::b2);
	
	// reset value already has default configuration of 
	// 8 bits data, no parity, and 1 stop bit.
	// disable Recieve interrupts
	USART2.Reset(USART::CR1, Register::b5);
	// Disable interrupt in NVIC ICER
	// NVIC ICER base address is 0xE000E180
	// there is 3 registers in NVIC ICER(0,1,2)
	// each register is at a 4 byte offset
	// USART2 is interrupt #38
        Register NVIC_ICER1(0xE000E184);
        NVIC_ICER1.Set(Register::b6);
}

void USART2_Driver::start()
{
	USART2.Set(USART::CR1, Register::b13); // set the Usart enable bit
}

void USART2_Driver::stop()
{
	USART2.Reset(USART::CR1, Register::b13); //clear the USART enable bit.
}

bool USART2_Driver::IsHeight()
{
	return height;
}

uint32_t USART2_Driver::GetPercent()
{
	return percent;
}



void USART2_Driver::USART2_IRQHandler(void)
{
	// if clear buffer, wipe before continuing
	if(clear_buffer)
	{
		for (int i = 0; i < RX_SIZE; i++)
    			RXBUFFER[i] = 0;
		rx_in_index = 0;
		clear_buffer = false;
	}
	uint32_t volatile IO_type = USART2.Read(USART::SR);
	// if interrupt is recieved
	if(IO_type & RXNE)
	{
		// clear the interrupt
		USART2.Reset(USART::SR, Register::b5);

		// read the input character
		uint8_t input = static_cast<uint8_t>(USART2.Read(USART::DR));

	if(input != ENTER)  
		{
			overflow = false;
			if(input == BKSP)
			{
				
				if(rx_in_index > 0)
				{
					rx_in_index--;
					SendByte(BKSP);
					if(rx_in_index < RX_SIZE)
					{
						RXBUFFER[rx_in_index] = 0;
					}
					else
					{
						overflow = true;
					}
				}
			}
			else // any other character
			{
				SendByte(input);
				if(rx_in_index < RX_SIZE)
				{
					if(rx_in_index < 0)
						rx_in_index = 0;
					RXBUFFER[rx_in_index] = input;
				}
				else
				{
					overflow = true;
				}
				rx_in_index++;
			}
		}
				// case of enter:
		else 
		{	
			if(!overflow)
			{
				// add null character if no overflow
				rx_in_index++;
				RXBUFFER[rx_in_index] = '\0';
			}
			else
			{ 	//truncate to 30 characters + null character
				// if overflowed
				RXBUFFER[RX_SIZE -1] = '\0';
			}
			// send the user command to the CLI task 
			xQueueSendFromISR( xCLIInQueue, &RXBUFFER, 0);
		}
	}
}
void USART2_IRQHandler(void)
{
        USART2_Driver::USART2_IRQHandler();
}


