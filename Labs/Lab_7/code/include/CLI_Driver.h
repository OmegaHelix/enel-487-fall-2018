#include "USART2_Driver.h"
#include "LED_Driver.h"
#include "TIM4_Driver.h"
#include "ADC2_Driver.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

//CLI_Driver is a USART2_Driver
class CLI_Driver: public USART2_Driver
{
	private:
	public:
		//Constructor and destructor
		CLI_Driver();
		~CLI_Driver();

		//Driver functions
		void open();
		void close();
		void start();
		void stop();
	
		//Usage functions
		void getLine();
		void sendLine(uint8_t*);
		void sendMenu();
		void newLine();
		void InterperetBuffer(uint8_t*);
	
		void getADCReading();
		void getTenReadings();
		void printDoubleCarrot();
		bool PWMbufferComp(uint8_t*,uint8_t*);	
		bool bufferComp(uint8_t*,uint8_t*);
		void sendHelp();
		void bufferClear();
		void CLI();
		void clear_buff();
		bool read_ready_buff();
		void ready_buff();

		void executeCommand(uint8_t*);
};

