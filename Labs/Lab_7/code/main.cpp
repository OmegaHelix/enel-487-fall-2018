/*
    FreeRTOS V7.2.0 - Copyright (C) 2012 Real Time Engineers Ltd.


    ***************************************************************************
     *                                                                       *
     *    FreeRTOS tutorial books are available in pdf and paperback.        *
     *    Complete, revised, and edited pdf reference manuals are also       *
     *    available.                                                         *
     *                                                                       *
     *    Purchasing FreeRTOS documentation will not only help you, by       *
     *    ensuring you get running as quickly as possible and with an        *
     *    in-depth knowledge of how to use FreeRTOS, it will also help       *
     *    the FreeRTOS project to continue with its mission of providing     *
     *    professional grade, cross platform, de facto standard solutions    *
     *    for microcontrollers - completely free of charge!                  *
     *                                                                       *
     *    >>> See http://www.FreeRTOS.org/Documentation for details. <<<     *
     *                                                                       *
     *    Thank you for using FreeRTOS, and thank you for your support!      *
     *                                                                       *
    ***************************************************************************


    This file is part of the FreeRTOS distribution.

    FreeRTOS is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License (version 2) as published by the
    Free Software Foundation AND MODIFIED BY the FreeRTOS exception.
    >>>NOTE<<< The modification to the GPL is included to allow you to
    distribute a combined work that includes FreeRTOS without being obliged to
    provide the source code for proprietary components outside of the FreeRTOS
    kernel.  FreeRTOS is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details. You should have received a copy of the GNU General Public
    License and the FreeRTOS license exception along with FreeRTOS; if not it
    can be viewed here: http://www.freertos.org/a00114.html and also obtained
    by writing to Richard Barry, contact details for whom are available on the
    FreeRTOS WEB site.

    1 tab == 4 spaces!
    
    ***************************************************************************
     *                                                                       *
     *    Having a problem?  Start by reading the FAQ "My application does   *
     *    not run, what could be wrong?                                      *
     *                                                                       *
     *    http://www.FreeRTOS.org/FAQHelp.html                               *
     *                                                                       *
    ***************************************************************************

    
    http://www.FreeRTOS.org - Documentation, training, latest information, 
    license and contact details.
    
    http://www.FreeRTOS.org/plus - A selection of FreeRTOS ecosystem products,
    including FreeRTOS+Trace - an indispensable productivity tool.

    Real Time Engineers ltd license FreeRTOS to High Integrity Systems, who sell 
    the code with commercial support, indemnification, and middleware, under 
    the OpenRTOS brand: http://www.OpenRTOS.com.  High Integrity Systems also
    provide a safety engineered and independently SIL3 certified version under 
    the SafeRTOS brand: http://www.SafeRTOS.com.
*/

/*
 * This is a very simple demo that demonstrates task and queue usages only in
 * a simple and minimal FreeRTOS configuration.  Details of other FreeRTOS 
 * features (API functions, tracing features, diagnostic hook functions, memory
 * management, etc.) can be found on the FreeRTOS web site 
 * (http://www.FreeRTOS.org) and in the FreeRTOS book.
 *
 * Details of this demo (what it does, how it should behave, etc.) can be found
 * in the accompanying PDF application note.
 *
*/

/* Kernel includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"

/* Standard include. */
#include <stdio.h>

/* Library includes */
#include "CLI_Driver.h"
#include "TIM4_Driver.h"
#include "ControllerStruct.h"
/* Priorities at which the tasks are created. */
#define mainQUEUE_RECEIVE_TASK_PRIORITY     ( tskIDLE_PRIORITY + 2 )
#define mainQUEUE_SEND_TASK_PRIORITY        ( tskIDLE_PRIORITY + 1 )
#define mainTASK1_PRIORITY                  ( tskIDLE_PRIORITY + 3 )
#define mainTASK0_PRIORITY                  ( tskIDLE_PRIORITY + 3 )

/* The rate at which data is sent to the queue, specified in milliseconds. */
#define mainQUEUE_SEND_FREQUENCY_MS         ( 10 / portTICK_RATE_MS )

/* The number of items the queue can hold.  This is 1 as the receive task
will remove items as they are added, meaning the send task should always find
the queue empty. */
#define mainQUEUE_LENGTH                    ( 3 )

/* The ITM port is used to direct the printf() output to the serial window in 
the Keil simulator IDE. */
#define mainITM_Port8(n)    (*((volatile unsigned char *)(0xE0000000+4*n)))
#define mainITM_Port32(n)   (*((volatile unsigned long *)(0xE0000000+4*n)))
#define mainDEMCR           (*((volatile unsigned long *)(0xE000EDFC)))
#define mainTRCENA          0x01000000

/*-----------------------------------------------------------*/

/*
 * The tasks as described in the accompanying PDF application note.
 */
static void prvQueueReceiveTask( void *pvParameters );
static void prvQueueSendTask( void *pvParameters );
static void task0( void *pvParameters );
static void task1( void *pvParameters );
static void CLITask( void *pvParameters );
static void ADCReadTask( void *pvParameters );
static void ControllerTask( void *pvParameters );

/*
 * Redirects the printf() output to the serial window in the Keil simulator
 * IDE.
 */
extern "C" {int fputc( int iChar, FILE *pxNotUsed );};

/*-----------------------------------------------------------*/

/* The queue used by both tasks. */
static xQueueHandle xControllerInQueue = NULL;

static CLI_Driver RTOSCLI;

/* One array position is used for each task created by this demo.  The 
variables in this array are set and cleared by the trace macros within
FreeRTOS, and displayed on the logic analyzer window within the Keil IDE -
the result of which being that the logic analyzer shows which task is
running when. */
unsigned long ulTaskNumber[ configEXPECTED_NO_RUNNING_TASKS ];

	
	//lut for mapping the height readings.
	static const uint16_t LUT[100] = {
		1442,1442,1442,1442,1442,1442,1442,1442, 	 /*8  0 to 5cm  */
		3174,3174,3174,3174,3174,3174,3174,3174, 	 /*16  5 to 10cm */
		2319,2319,2319,2319,2319,2319,2319,2319, 	 /*24 10 to 15cm */
		2070,2070,2070,2070,2070,2070,2070,2070, 	 /*32 15 to 20cm */
		1695,1695,1695,1695,1695,1695,1695,1695, 	 /*40 20 to 25cm */
		1046,1046,1046,1046,1046,1046,1046,1046, 	 /*48 25 to 30cm */
		789,789,789,789,789,789,789,789, 			     /*56 30 to 35cm */
		721,721,721,721,721,721,721,721,721, 		   /*65 35 to 40cm */ 
		686,686,686,686,686,686,686,686,686,		   /*74 40 to 45cm */ 
		666,666,666,666,666,666,666,666,666, 		   /*83 45 to 50cm */ 
		627,627,627,627,627,627,627,627,627,		   /*92 50 to 55cm */ 
		552,552,552,552,552,552,552,552  			     /*100 55 to 60cm */
	};




/*-----------------------------------------------------------*/
#include "main.h"

int main(void)
{
    // Copied from blinkOneLight project
    // ----------------------------------------------------------------
    setupRegs();
		
		RTOSCLI.open();
		RTOSCLI.start();
    //Need to setup the the LED's on the board
    * regRCC_APB2ENR |= 0x08; // Enable Port B clock
    * regGPIOB_ODR  &= ~0x0000FF00;          /* switch off LEDs                    */
    * regGPIOB_CRH  = 0x33333333; //50MHz  General Purpose output push-pull

    //Writing to this register will turn a light on. Bits 8-15 control the bank of lights
    * regGPIOB_BSRR = 1u<<8;  //Set bit 8
    * regGPIOB_BSRR = 1u<<11; // set bit 11
    //----------------------------------------------------------------

    /* Create the queue. */
    xControllerInQueue = xQueueCreate( mainQUEUE_LENGTH, sizeof( ControllerData ) );
	  USART2_Driver::xCLIInQueue = xQueueCreate( mainQUEUE_LENGTH + 2, sizeof( char[31] ) );
			
    printf("I am groot\n");
    if( xControllerInQueue != NULL )
    {
        /* Start the two tasks as described in the accompanying application
        note. */
//			 xTaskCreate( prvQueueReceiveTask, ( const char * ) "Rx",
//								configMINIMAL_STACK_SIZE, NULL,
//								mainQUEUE_RECEIVE_TASK_PRIORITY, NULL );
			 xTaskCreate( CLITask, ( const char * ) "CLI",
								configMINIMAL_STACK_SIZE, NULL,
								mainTASK0_PRIORITY, NULL );

        xTaskCreate( ADCReadTask, (const char *) "ADC",
                 configMINIMAL_STACK_SIZE, NULL,
                 mainTASK0_PRIORITY + 1, NULL );
        xTaskCreate( ControllerTask, (const char *) "Controller",
                 configMINIMAL_STACK_SIZE, NULL,
                 mainTASK0_PRIORITY + 2, NULL );
        /* Start the tasks running. */
        vTaskStartScheduler();
    }

    /* If all is well we will never reach here as the scheduler will now be
    running.  If we do reach here then it is likely that there was insufficient
    heap available for the idle task to be created. */
    for( ;; )
        ;
}
/*-----------------------------------------------------------*/

static void task0( void *pvParameters )
{
    portTickType xNextWakeTime = xTaskGetTickCount();
    
    while (1) {
        * regGPIOB_BSRR = 1u<<11; // set bit 11
        for (int i=0; i < 512000; ++i)
        {} // work
        * regGPIOB_BSRR = 1u<<27; // clr bit 11
        vTaskDelayUntil(&xNextWakeTime, 200);
    }

}
static void CLITask( void *pvParameters )
{
    portTickType xNextWakeTime = xTaskGetTickCount();
		uint8_t userCommand[31];
		ControllerData cd;


    while (1) {
			
			    	// get new data from the queue
						xQueueReceive( USART2_Driver::xCLIInQueue , &userCommand, portMAX_DELAY );
						// interpret the command
						RTOSCLI.InterperetBuffer(userCommand);
						// pipe out the menu
						RTOSCLI.newLine();
						RTOSCLI.sendMenu();
						// wipe buffer
						RTOSCLI.clear_buff();
						if(RTOSCLI.IsHeight())
						{
							cd.source = ControllerData::CLI;				
							cd.percent = RTOSCLI.GetPercent();
							xQueueSend( xControllerInQueue, &cd, 0 );
						}
						vTaskDelayUntil(&xNextWakeTime, 10);
    }

}

static void ADCReadTask( void *pvParameters )
{
    portTickType xNextWakeTime = xTaskGetTickCount();
		ControllerData cd;
		cd.source = ControllerData::ADC;
    while (1) {
			//tell the adc we want another reading
			ADC1_Driver::ADC1_start_reading();
			//wait for the reading to be ready
			while(!ADC1_Driver::get_ADC1_data_ready()){};
				//get the reading
				cd.adc_value = ADC1_Driver::getVoltage();
				
				// mark that we've gotten this reading
				// and that we need a new reading before
				// we care about the data.
				ADC1_Driver::set_ADC1_data_ready(false);
				// send voltage to Controller here in queue		
				xQueueSend( xControllerInQueue, &cd, 0 );
        vTaskDelayUntil(&xNextWakeTime, 1);
    }

}


static void ControllerTask( void *pvParameters )
{
    portTickType xNextWakeTime = xTaskGetTickCount();
    static int degree = 12;
		static int index = 50;
		ControllerData cdRecieve;
	  RTOSCLI.newLine();
		// pipe out the menu
		RTOSCLI.sendMenu();
	
    while (1) {
			// get new data from the queue
			xQueueReceive( xControllerInQueue, &cdRecieve, portMAX_DELAY );
				

			if (cdRecieve.source == ControllerData::CLI)
			{
				index = cdRecieve.percent;
			}		
			// if the data is for the ADC
			else if(cdRecieve.source == ControllerData::ADC)
			{
				// if the desired reading is higher
				// than the actual reading
				// and the degree hasn't exceeded the max 180
				// replace static variable with value from index of LUT
				// LUT[% from cli]
				// 27 top, 20 bot
				if((cdRecieve.adc_value > LUT[index])  && degree < 27)
					degree++;
				//otherwise lower the degree unless it's already at 0
				else if(degree > 2s0)
					degree--;
				
				if(cdRecieve.adc_value < 500 && degree != 12)
					degree = 16;
				TIM4_Driver::SetPeriod(degree);
			}

    }

}




static void prvQueueSendTask( void *pvParameters )
{
portTickType xNextWakeTime;
const unsigned long ulValueToSend = 100UL;

    /* Initialise xNextWakeTime - this only needs to be done once. */
    xNextWakeTime = xTaskGetTickCount();

    for( ;; )
    {
        /* Place this task in the blocked state until it is time to run again.
        The block time is specified in ticks, the constant used converts ticks
        to ms.  While in the Blocked state this task will not consume any CPU
        time. */
        vTaskDelayUntil( &xNextWakeTime, mainQUEUE_SEND_FREQUENCY_MS );

        /* Send to the queue - causing the queue receive task to unblock and
        print out a message.  0 is used as the block time so the sending 
        operation will not block - it shouldn't need to block as the queue 
        should always be empty at this point in the code. */
        xQueueSend( xControllerInQueue, &ulValueToSend, 0 );
    }
}
/*-----------------------------------------------------------*/

static void prvQueueReceiveTask( void *pvParameters )
{
unsigned long ulReceivedValue;

    for( ;; )
    {
        /* Wait until something arrives in the queue - this task will block
        indefinitely provided INCLUDE_vTaskSuspend is set to 1 in
        FreeRTOSConfig.h. */
        xQueueReceive( xControllerInQueue, &ulReceivedValue, portMAX_DELAY );

        /*  To get here something must have been received from the queue, but
        is it the expected value?  If it is, print out a pass message, if no,
        print out a fail message. */
        if( ulReceivedValue == 100UL )
        {
            printf( "Value 100 received - Tick = 0x%x\r\n", xTaskGetTickCount() );
        }
        else
        {
            printf( "Received an unexpected value\r\n" );
        }
    }
}
/*-----------------------------------------------------------*/

int fputc( int iChar, FILE *pxNotUsed ) 
{
    /* Just to avoid compiler warnings. */
    ( void ) pxNotUsed;

    if( mainDEMCR & mainTRCENA ) 
    {
        while( mainITM_Port32( 0 ) == 0 );
        mainITM_Port8( 0 ) = iChar;
    }

    return( iChar );
}
