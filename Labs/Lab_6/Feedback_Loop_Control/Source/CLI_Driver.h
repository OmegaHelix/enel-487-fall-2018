#include "USART2_Driver.h"
#include "LED_Driver.h"
#include "TIM4_Driver.h"
#include "ADC2_Driver.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

//CLI_Driver is a USART2_Driver
class CLI_Driver: public USART2_Driver
{
	private:
	public:
		//Constructor and destructor
		CLI_Driver();
		~CLI_Driver();

		//Driver functions
		void open();
		void close();
		void start();
		void stop();
	
		//Useage functions
		void getLine();
		void sendLine(uint8_t*);
		void sendMenu();
		void newLine();
		void InterperetBuffer();
	
		void getADCReading();
		void getTenReadings();
		void printDoubleCarrot();
		bool PWMBufferComp(uint8_t*);	
		bool bufferComp(uint8_t*);
		void sendHelp();
		void bufferClear();
		void CLI();
		bool UpdateHeight(uint8_t* );

		void executeCommand(uint8_t*);
		
		static uint16_t height;
};

