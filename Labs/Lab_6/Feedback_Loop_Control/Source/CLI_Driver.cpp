#include "CLI_Driver.h"

	uint16_t CLI_Driver::height = 0;
	
	//lut for mapping the height readings.
	static const uint16_t LUT[100] = {
		1442,1442,1442,1442,1442,1442,1442,1442, 	 /*8  0 to 5cm  */
		3174,3174,3174,3174,3174,3174,3174,3174, 	 /*16  5 to 10cm */
		2319,2319,2319,2319,2319,2319,2319,2319, 	 /*24 10 to 15cm */
		2070,2070,2070,2070,2070,2070,2070,2070, 	 /*32 15 to 20cm */
		1695,1695,1695,1695,1695,1695,1695,1695, 	 /*40 20 to 25cm */
		1046,1046,1046,1046,1046,1046,1046,1046, 	 /*48 25 to 30cm */
		789,789,789,789,789,789,789,789, 			 /*56 30 to 35cm */
		721,721,721,721,721,721,721,721,721, 		 /*65 35 to 40cm */ 
		686,686,686,686,686,686,686,686,686,		 /*74 40 to 45cm */ 
		666,666,666,666,666,666,666,666,666, 		 /*83 45 to 50cm */ 
		627,627,627,627,627,627,627,627,627,		 /*92 50 to 55cm */ 
		552,552,552,552,552,552,552,552  			 /*100 55 to 60cm */
	};

	static uint8_t HELP[]              = "HELP";
	static uint8_t HELP_CASE[]        = "Commands are case insensitive";
	static uint8_t HELP_LINE[]         = "The possible commands are:";
	static uint8_t HELP_DESC[]         = "\tHELP provides the above list of command descriptions";
	static uint8_t LED_LINE[]          = "LED <# 1 through 8 or ALL> <ON, OFF, or STATE>";
	static uint8_t LED_ON_DESC_1[]     = "\tLED # ON\t provides power to the LED";
	static uint8_t LED_ON_DESC_2[]     = "\tLED ALL ON\t provides power to all LED's";	
	static uint8_t LED_OFF_DESC_1[]    = "\tLED # OFF\t turns off power to the LED";
	static uint8_t LED_OFF_DESC_2[]    = "\tLED ALL OFF\t turns off power to all LED's";	
	static uint8_t LED_STATE_DESC_1[]  = "\tLED # STATE\t provides power status of the LED";
	static uint8_t LED_STATE_DESC_2[]  = "\tLED ALL STATE\t provides power status of all LED's";
	static uint8_t SERVO_DESC_1[]  			 = "SERVO < degrees or percent value>";
	static uint8_t SERVO_DESC_2[]  			 = "\tDefault unit is degrees, use a % sign to use percentage unit";
	static uint8_t SERVO_DESC_PERCENT[]  = "\tSERVO #%\t #'s value must be in range of 0% to 100% ";
	static uint8_t SERVO_DESC_DEGREE[]   = "\tSERVO # \t #'s value must be in range of 0  to 180 degrees";
	static uint8_t INVALID_VALUE[]       = "Invalid value, check HELP for allowed values";
	static uint8_t ADC_DESC[]						 = "ADC <READ or BULK>";
	static uint8_t ADC_DESC_BULK[]			 = "\tADC BULK\t Will print 10 closely gathered ADC readings";
	static uint8_t ADC_DESC_READ[]			 = "\tADC READ\t Will print a single ADC reading";

	
	static uint8_t COMPILE_TIME[]      = "COMPILE TIME";	
	static uint8_t COMPILE_TIME_DESC[] = "\tCOMPILE TIME displays the date and time the source file was compiled";
	static uint8_t COMPILE_LINE[]      = "Compiled at: " __DATE__ " " __TIME__;
	
	static uint8_t LED_ALL_ON[]        = "LED ALL ON";
	static uint8_t LED_1_ON[]  	   = "LED 1 ON";
	static uint8_t LED_2_ON[]   	   = "LED 2 ON";
	static uint8_t LED_3_ON[]      	   = "LED 3 ON";
	static uint8_t LED_4_ON[]      	   = "LED 4 ON";
	static uint8_t LED_5_ON[]      	   = "LED 5 ON";
	static uint8_t LED_6_ON[]     	   = "LED 6 ON";
	static uint8_t LED_7_ON[]      	   = "LED 7 ON";
	static uint8_t LED_8_ON[]      	   = "LED 8 ON";
	
	static uint8_t LED_ON_CONF[]   	   = "LED Turned on";
	
	
	static uint8_t LED_ALL_OFF[]       = "LED ALL OFF";
	static uint8_t LED_1_OFF[]         = "LED 1 OFF";
	static uint8_t LED_2_OFF[]         = "LED 2 OFF";
	static uint8_t LED_3_OFF[]         = "LED 3 OFF";
	static uint8_t LED_4_OFF[]         = "LED 4 OFF";
	static uint8_t LED_5_OFF[]         = "LED 5 OFF";
	static uint8_t LED_6_OFF[]         = "LED 6 OFF";
	static uint8_t LED_7_OFF[]         = "LED 7 OFF";
	static uint8_t LED_8_OFF[]         = "LED 8 OFF";
	
	static uint8_t LED_OFF_CONF[]      = "LED Turned off";
	
	
	static uint8_t LED_ALL_STATE[]     = "LED ALL STATE";
	static uint8_t LED_1_STATE[]       = "LED 1 STATE";
	static uint8_t LED_2_STATE[]       = "LED 2 STATE";
	static uint8_t LED_3_STATE[]       = "LED 3 STATE";
	static uint8_t LED_4_STATE[]       = "LED 4 STATE";
	static uint8_t LED_5_STATE[]       = "LED 5 STATE";
  	static uint8_t LED_6_STATE[]     = "LED 6 STATE";
	static uint8_t LED_7_STATE[]       = "LED 7 STATE";
	static uint8_t LED_8_STATE[]       = "LED 8 STATE";

	static uint8_t LED_STATE_ON[]      = "LED is on";
	static uint8_t LED_STATE_OFF[]     = "LED is off";
	static uint8_t INVALID_COMMAND[]   = "Invalid Command";
	
	static uint8_t SERVO[]             = "SERVO";
	
	static uint8_t ADCBULK[]				 = "ADC BULK"; 
		static uint8_t ADCSINGLE[]				 = "ADC READ"; 
	static uint8_t UPDATEHEIGHT[]			= "UPDATE HEIGHT";

CLI_Driver::CLI_Driver(): USART2_Driver(){};
CLI_Driver::~CLI_Driver(){}

void CLI_Driver::open(){
        // open the USART2 device
        USART2_Driver::open();
        //open the LED_Driver
        LED_Driver::open();
				TIM4_Driver::open();
				ADC1_Driver::open();
}


void CLI_Driver::close(){
        USART2_Driver::close();
        LED_Driver::close();
}


void CLI_Driver::start(){
        USART2_Driver::start();
        LED_Driver::start();
}


void CLI_Driver::stop(){
        USART2_Driver::stop();
        LED_Driver::stop();
}	


void CLI_Driver::CLI()
{
					//SWSTART 
			ADC1_Driver::ADC1.Set(ADC::CR2, Register::b22);
        newLine();
		// pipe out the menu
		sendMenu();
        while(1)
        {
					// if there's a finished command,
					if(ready_buffer)
					{
						// interpret the command
						InterperetBuffer();
						// pipe out the menu
						newLine();
						sendMenu();
						// wipe buffer
						
						clear_buffer = true;
						ready_buffer = false;
					}
        }
}		

void CLI_Driver::getLine()
{
	newLine();
	bufferClear();
	printDoubleCarrot();
}

void CLI_Driver::sendLine(uint8_t* output)
{
	newLine();
	uint32_t i = 0;
	while(output[i] != '\0')
	{
		SendByte(output[i]);
		i++;
	}
}

void CLI_Driver::newLine()
{
	SendByte(0xA);
	SendByte(0xD);
}

void CLI_Driver::sendMenu()
{
	newLine();
	uint8_t Menu_1[] = "Enter a Command below. Use HELP to see a list of commands";
	sendLine(Menu_1);
	newLine();
	printDoubleCarrot();

}

void CLI_Driver::InterperetBuffer()
{
	uint8_t overflow_message[] = "Command too long, truncated to 30 characters";
	bool state = false;
	for (int i = 0; RXBUFFER[i] != '\0'; i++)
	{
		RXBUFFER[i] = toupper(RXBUFFER[i]);
	}
	if(overflow)
		sendLine(overflow_message);
	if(bufferComp(HELP))
		sendHelp();
	else if (bufferComp(COMPILE_TIME))
	{
			sendLine(COMPILE_LINE);
	}
	else if (bufferComp(LED_ALL_ON))
	{
			LED_Driver::SetLED(LED_Driver::LED_ALL);
			sendLine(LED_ON_CONF);
	}
	else if (bufferComp(LED_1_ON))
	{
			LED_Driver::SetLED(LED_Driver::LED_1);
			sendLine(LED_ON_CONF);
	}
	else if (bufferComp(LED_2_ON))
	{
			LED_Driver::SetLED(LED_Driver::LED_2);
			sendLine(LED_ON_CONF);
	}
	else if (bufferComp(LED_3_ON))
	{
			LED_Driver::SetLED(LED_Driver::LED_3);
			sendLine(LED_ON_CONF);
	}
	else if (bufferComp(LED_4_ON))
	{
			LED_Driver::SetLED(LED_Driver::LED_4);
			sendLine(LED_ON_CONF);
	}
	else if (bufferComp(LED_5_ON))
	{
			LED_Driver::SetLED(LED_Driver::LED_5);
			sendLine(LED_ON_CONF);
	}
	else if (bufferComp(LED_6_ON))
	{
			LED_Driver::SetLED(LED_Driver::LED_6);
			sendLine(LED_ON_CONF);
	}
	else if (bufferComp(LED_7_ON))
	{
			LED_Driver::SetLED(LED_Driver::LED_7);
			sendLine(LED_ON_CONF);
	}
	else if (bufferComp(LED_8_ON))
	{
			LED_Driver::SetLED(LED_Driver::LED_8);
			sendLine(LED_ON_CONF);
	}
	
	else if (bufferComp(LED_ALL_OFF))
	{
			LED_Driver::ResetLED(LED_Driver::LED_ALL);
			sendLine(LED_OFF_CONF);
	}
	else if (bufferComp(LED_1_OFF))
	{
			LED_Driver::ResetLED(LED_Driver::LED_1);
			sendLine(LED_OFF_CONF);
	}
	else if (bufferComp(LED_2_OFF))
	{
			LED_Driver::ResetLED(LED_Driver::LED_2);
			sendLine(LED_OFF_CONF);
	}
	else if (bufferComp(LED_3_OFF))
	{
			LED_Driver::ResetLED(LED_Driver::LED_3);
			sendLine(LED_OFF_CONF);
	}
	else if (bufferComp(LED_4_OFF))
	{
			LED_Driver::ResetLED(LED_Driver::LED_4);
			sendLine(LED_OFF_CONF);
	}
	else if (bufferComp(LED_5_OFF))
	{
			LED_Driver::ResetLED(LED_Driver::LED_5);
			sendLine(LED_OFF_CONF);
	}
	else if (bufferComp(LED_6_OFF))
	{
			LED_Driver::ResetLED(LED_Driver::LED_6);
			sendLine(LED_OFF_CONF);
	}
	else if (bufferComp(LED_7_OFF))
	{
			LED_Driver::ResetLED(LED_Driver::LED_7);
			sendLine(LED_OFF_CONF);
	}
	else if (bufferComp(LED_8_OFF))
	{
			LED_Driver::ResetLED(LED_Driver::LED_8);
			sendLine(LED_OFF_CONF);
	}

	
	
	else if (bufferComp(LED_ALL_STATE))
	{
	}
	else if (bufferComp(LED_1_STATE))
	{
			state = LED_Driver::StateLED(LED_Driver::LED_1);
			if(state)
				sendLine(LED_STATE_ON);
			else
				sendLine(LED_STATE_OFF);
	}
	else if (bufferComp(LED_2_STATE))
	{
			state = LED_Driver::StateLED(LED_Driver::LED_2);
			if(state)
				sendLine(LED_STATE_ON);
			else
				sendLine(LED_STATE_OFF);
	}
	else if (bufferComp(LED_3_STATE))
	{
			state = LED_Driver::StateLED(LED_Driver::LED_3);
			if(state)
				sendLine(LED_STATE_ON);
			else
				sendLine(LED_STATE_OFF);
	}
	else if (bufferComp(LED_4_STATE))
	{
			state = LED_Driver::StateLED(LED_Driver::LED_4);
			if(state)
				sendLine(LED_STATE_ON);
			else
				sendLine(LED_STATE_OFF);
	}
	else if (bufferComp(LED_5_STATE))
	{
			state = LED_Driver::StateLED(LED_Driver::LED_5);
			if(state)
				sendLine(LED_STATE_ON);
			else
				sendLine(LED_STATE_OFF);
	}
	else if (bufferComp(LED_6_STATE))
	{
			state = LED_Driver::StateLED(LED_Driver::LED_6);
			if(state)
				sendLine(LED_STATE_ON);
			else
				sendLine(LED_STATE_OFF);
	}
	else if (bufferComp(LED_7_STATE))
	{
			state = LED_Driver::StateLED(LED_Driver::LED_7);
			if(state)
				sendLine(LED_STATE_ON);
			else
				sendLine(LED_STATE_OFF);
	}
	else if (bufferComp(LED_8_STATE))
	{
			state = LED_Driver::StateLED(LED_Driver::LED_8);
			if(state)
				sendLine(LED_STATE_ON);
			else
				sendLine(LED_STATE_OFF);
	}
		else if(bufferComp(ADCBULK))
	{
		getTenReadings();
	}
		else if(bufferComp(ADCSINGLE))
	{
		getADCReading();
	}
		else if (UpdateHeight(UPDATEHEIGHT))
		{
		}
	else if(PWMBufferComp(SERVO))
	{
	}
		else
		{
			if(RXBUFFER[0] != '\0')
			sendLine(INVALID_COMMAND);
		}															
}

void CLI_Driver::printDoubleCarrot()
{
	SendByte('>');
	SendByte('>');
}

void CLI_Driver::bufferClear()
{
	// set the buffer to 0;
	for( int i = 0; i < RX_SIZE; i++)
		RXBUFFER[i] = 0x0;
}

bool CLI_Driver::bufferComp(uint8_t* command)
{
	int32_t i = 0;
	// escapes when one of the two strings reads an end of string character
	while((RXBUFFER[i] != '\0') && (command[i] != '\0'))
	{
		if(RXBUFFER[i] != command[i])
			return false;
		i++;
	}
	// if both strings are on an end of string character
	if((RXBUFFER[i] == '\0') && (command[i] == '\0'))
	return true;
	
	// else return false
	else return false;
}

bool CLI_Driver::PWMBufferComp(uint8_t* command)
{
	char* endptr;
	int32_t degree = strncmp(reinterpret_cast<const char *>(command), 
											reinterpret_cast<const char *>(RXBUFFER), 4);
	if(degree == 0 && 0 != isdigit(static_cast<int32_t>(*(RXBUFFER + 6))))
	{
		degree = static_cast<uint16_t>(
						strtol(reinterpret_cast<const char *>(RXBUFFER + 5), &endptr, 10));
		if(*endptr == '%') 
		{
			if(degree >= 0 && degree <= 100)
			{
				degree = degree * 1.8;
				TIM4_Driver::SetPeriod(degree);
				return true;
			}
			else
			{
				sendLine(INVALID_VALUE);
				// else return false
				return false;
			}
		}
		else if(degree <=180 && degree >= 0)
		{
			TIM4_Driver::SetPeriod(degree);
			return true;
		}

	}
		//sendLine(INVALID_VALUE);
		// else return false
		return false;
}


bool CLI_Driver::UpdateHeight(uint8_t* command)
{
	char* endptr;
	int32_t degree = strncmp(reinterpret_cast<const char *>(command), 
											reinterpret_cast<const char *>(RXBUFFER), 13);
	if(degree == 0 && 0 != isdigit(static_cast<int32_t>(*(RXBUFFER + 14))))
	{
		degree = static_cast<uint16_t>(
						strtol(reinterpret_cast<const char *>(RXBUFFER + 14), &endptr, 10));

			if(degree >= 0 && degree <= 100)
			{
				degree = degree;
				CLI_Driver::height = degree;
				return true;
			}
			else
			{
				sendLine(INVALID_VALUE);
			}
	}
	return false;
}




void CLI_Driver::sendHelp()
{
	sendLine(HELP_CASE);
	sendLine(HELP_LINE);
	sendLine(LED_LINE);
	sendLine(LED_ON_DESC_1);
	sendLine(LED_ON_DESC_2);
	sendLine(LED_OFF_DESC_1);
	sendLine(LED_OFF_DESC_2);
	sendLine(LED_STATE_DESC_1);
	sendLine(LED_STATE_DESC_2);
	sendLine(COMPILE_TIME);
	sendLine(COMPILE_TIME_DESC);
	sendLine(HELP);
	sendLine(HELP_DESC);
	sendLine(SERVO_DESC_1);
	sendLine(SERVO_DESC_2);
	sendLine(SERVO_DESC_PERCENT);
	sendLine(SERVO_DESC_DEGREE);
	sendLine(ADC_DESC);
	sendLine(ADC_DESC_READ);
	sendLine(ADC_DESC_BULK);
	
	
}



void CLI_Driver::getTenReadings()
{
	if(ADC1_Driver::ADC1_data_ready )
						{
							for(int i = 0; i < 10; i++){
									while(!ADC1_Driver::ADC1_data_ready){};
									getADCReading();
								}
					}
}

void CLI_Driver::getADCReading()
{
	uint8_t voltage[30]; 
	sprintf(reinterpret_cast<char *>(voltage), "%d", 
		static_cast<int>(ADC1_Driver::getVoltage()));
	strcat(reinterpret_cast<char*>(voltage), " mV");
	sendLine(voltage);
	ADC1_Driver::ADC1_data_ready = false;
}


