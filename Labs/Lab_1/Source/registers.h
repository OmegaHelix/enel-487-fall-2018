#ifndef REGISTERS_H
#define REGISTERS_H
#include <stdint.h>



#define PERIPH_BASE              ((uint32_t)0x40000000)
#define AHBPERIPH_BASE           (PERIPH_BASE + 0x20000)
#define RCC_BASE                 (AHBPERIPH_BASE + 0x1000)
#define regRCC_APB2ENR           ((volatile uint32_t *)(RCC_BASE + 0x18))
#define APB2PERIPH_BASE          (PERIPH_BASE + 0x10000)
#define GPIOB_BASE               (APB2PERIPH_BASE + 0x0C00)
#define regGPIOB_ODR             ((volatile uint32_t *)(GPIOB_BASE + 0x0C))
#define regGPIOB_CRH             ((volatile uint32_t *)(GPIOB_BASE + 0x04))
#define regGPIOB_BSRR            ((volatile uint32_t *)(GPIOB_BASE  + 0x10))
#define regGPIOB_BRR             ((volatile uint32_t *)(GPIOB_BASE  + 0x14))
#define GPIOB_BxRR_LED_OFFSET			(0x7)


// Dynamic function to set any of the led's
void set_led(uint32_t);

// Dynamic function to reset any of the led's
	
void reset_led(uint32_t);

void setup_leds(void);

#endif      
