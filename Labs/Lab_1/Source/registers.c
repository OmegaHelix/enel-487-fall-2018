#include "registers.h"

// Dynamic function to set any of the led's

void set_led(uint32_t led_num)
{
		//bits 8-15 control the led
		//since LED '1' is in the 0th bit, shift by one less than the desired led
		// 7 is required as it's the offset for the led's
		*regGPIOB_BSRR = (0x1 << (led_num + GPIOB_BxRR_LED_OFFSET));
}

// Dynamic function to reset any of the led's
	
void reset_led(uint32_t led_num)
{
		//bits 8-15 control the led
		//since LED '1' is in the 0th bit, shift by one less than the desired led
		*regGPIOB_BRR = (0x1 << (led_num + GPIOB_BxRR_LED_OFFSET)); 
}

void setup_leds(void)
{
			//Need to setup the the LED's on the board
		* regRCC_APB2ENR |= 0x08; // Enable Port B clock
		* regGPIOB_ODR  &= ~0x0000FF00; // switch off LEDs                    
		* regGPIOB_CRH  = 0x33333333; //50MHz  General Purpose output push-pull
}

