#include "TIM4_Driver.h"
#include "LED_Driver.h"
const uint32_t TIM4_Driver::TIM4_BASE = 0x40000800;
TIMER TIM4_Driver::TIM4(TIM4_BASE);


TIM4_Driver::TIM4_Driver(){};
TIM4_Driver::~TIM4_Driver(){};
	
	void TIM4_Driver::open()
	{
		// Enable the AFIO clock
		RCC::Or(RCC::APB2ENR, RCC::_APB2ENR_AFIOEN);
		//Enable the GPIOB Clock
		RCC::Or(RCC::APB2ENR, RCC::_APB2ENR_IOPBEN);
		//Enable the TIM4 clock
		RCC::Or(RCC::APB1ENR, RCC::_APB1ENR_TIM4EN);
		
		
		GPIO::Write(GPIO::B, GPIO_PORT::CRL, 0xBu << 28);

		// these are data registers, should be written to.
		TIM4.Write(TIMER::PSC, 71); // set to 1us/tick
		TIM4.Write(TIMER::ARR, 20000); //2ms period
		TIM4.Write(TIMER::CCR2, 900); //duty cycle default to 90 degrees
		
		//Set OC2M for PWM Mode 1
		TIM4.Set(TIMER::CCMR1, Register::b14);
		TIM4.Set(TIMER::CCMR1, Register::b13);
			
		//enable Capture compare for CCR2
		TIM4.Set(TIMER::CCER, Register::b4);
		// set URS bit to 1
		TIM4.Set(TIMER::CR1, Register::b2);
		//Set CEN bit to 1
		TIM4.Set(TIMER::CR1, Register::b0);
		

	}		
	
	void TIM4_Driver::close(){};
	
	
	void TIM4_Driver::start(){};
	
	
	void TIM4_Driver::stop(){};

		
		void TIM4_Driver::SetPeriod(uint16_t degree)
		{
			uint16_t value = 600 + (degree * 10);
			TIM4.Write(TIMER::CCR2, value);
		}

