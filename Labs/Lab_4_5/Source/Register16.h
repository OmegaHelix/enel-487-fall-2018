#ifndef __REG16
#define __REG16
/******************************************************************************
* File: Register16.h
* 
* Description: 	This file holds the basic reigster class, a Register16 should be
*		written to and read. Other classes can expand upon this class.
*
* Member Variables:
*		reg:
*			a constant reference to a volatile unsigned integer
*
* Member Functions:
*	Register16(uint32_t):
*		This function Creates an instance of a Register16 from the given
*		unisgned integer as the address.
*
*	void Or(uint32_t Value);
*		This function takes in a value to Or into the Register16.
*
*	void And(uint32_t Value);
*		This function takes in a value to And with the Register16.
*
*	uint32_t Read(void);
*		This function returns the value of the Register16.
*
*
******************************************************************************/
#include "stdlib.h"
#include "stdint.h"
class Register16
{
	private: 
	// reference to a volatile UInt
	// Can be referenced as Register16 to be written to.
		uint16_t volatile & reg; 
	public:
		enum bit {b0 = 0,b1,b2,b3,b4,b5,b6,b7,b8,b9,b10,
                        b11,b12,b13,b14,b15};
			
		Register16(uint16_t);
		~Register16();
		void Or(uint16_t);
		void And(uint16_t);
		uint16_t Read(void);
		void Write(uint16_t);
		void Set(enum Register16::bit bit);
        void Reset(enum Register16::bit bit);
};

#endif


