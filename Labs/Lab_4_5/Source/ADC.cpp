#include "ADC.h"

//
//  Just made an ADC class
//  That can be used to make any adc
//
//
//
//
//






ADC::ADC(uint32_t _BASE):   _SR    (_BASE + 0x00),
														_CR1   (_BASE + 0x04),
														_CR2   (_BASE + 0x08),
														_SMPR1 (_BASE + 0x0C),
														_SMPR2 (_BASE + 0x10),
														_JOFR1 (_BASE + 0x14),
														_JOFR2 (_BASE + 0x18),
														_JOFR3 (_BASE + 0x1C),
														_JOFR4 (_BASE + 0x20),
														_HTR   (_BASE + 0x24),
														_LTR   (_BASE + 0x28),
														_SQR1  (_BASE + 0x2C),
														_SQR2  (_BASE + 0x30),
														_SQR3  (_BASE + 0x34),
														_JSQR  (_BASE + 0x38),
														_JDR1  (_BASE + 0x3C),
														_JDR2  (_BASE + 0x40),
														_JDR3  (_BASE + 0x44),
														_JDR4  (_BASE + 0x48),
														_DR    (_BASE + 0x4C){};
ADC::~ADC(){};

	
void ADC::Or(enum ADC::ADC_ reg, uint32_t value)
{
	switch (reg)
	{
		case SR:
			_SR.Or(value);
			break;
		case CR1:
			_CR1.Or(value);
			break;
		case CR2:
			_CR2.Or(value);
			break;
		case SMPR1:
			_SMPR1.Or(value);
			break;
		case SMPR2: 
			_SMPR2.Or(value);
			break;
		case JOFR1: 
			_JOFR1.Or(value);
			break;
		case JOFR2: 
			_JOFR2.Or(value);
			break;
		case JOFR3: 
			_JOFR3.Or(value);
			break;
		case JOFR4: 
			_JOFR4.Or(value);
			break;
		case HTR: 
			_HTR.Or(value);
			break;
		case LTR: 
			_LTR.Or(value);
			break;
		case SQR1: 
			_SQR1.Or(value);
			break;
		case SQR2: 
			_SQR2.Or(value);
			break;
		case SQR3: 
			_SQR3.Or(value);
			break;
		case JSQR: 
			_JSQR.Or(value);
			break;
		case JDR1: 
			_JDR1.Or(value);
			break;
		case JDR2: 
			_JDR2.Or(value);
			break;
		case JDR3: 
			_JDR3.Or(value);
			break;
		case JDR4: 
			_JDR4.Or(value);
			break;
		case DR: 
			_DR.Or(value);
			break;
		
	};
};
void ADC::And(enum ADC::ADC_ reg, uint32_t value)
{
	switch (reg)
	{
	case SR:
			_SR.And(value);
			break;
		case CR1:
			_CR1.And(value);
			break;
		case CR2:
			_CR2.And(value);
			break;
		case SMPR1:
			_SMPR1.And(value);
			break;
		case SMPR2: 
			_SMPR2.And(value);
			break;
		case JOFR1: 
			_JOFR1.And(value);
			break;
		case JOFR2: 
			_JOFR2.And(value);
			break;
		case JOFR3: 
			_JOFR3.And(value);
			break;
		case JOFR4: 
			_JOFR4.And(value);
			break;
		case HTR: 
			_HTR.And(value);
			break;
		case LTR: 
			_LTR.And(value);
			break;
		case SQR1: 
			_SQR1.And(value);
			break;
		case SQR2: 
			_SQR2.And(value);
			break;
		case SQR3: 
			_SQR3.And(value);
			break;
		case JSQR: 
			_JSQR.And(value);
			break;
		case JDR1: 
			_JDR1.And(value);
			break;
		case JDR2: 
			_JDR2.And(value);
			break;
		case JDR3: 
			_JDR3.And(value);
			break;
		case JDR4: 
			_JDR4.And(value);
			break;
		case DR: 
			_DR.And(value);
			break;
	};
};

void ADC::Set(enum ADC::ADC_ reg, Register::bit bit)
{
	switch (reg)
	{
	case SR:
			_SR.Set(bit);
			break;
		case CR1:
			_CR1.Set(bit);
			break;
		case CR2:
			_CR2.Set(bit);
			break;
		case SMPR1:
			_SMPR1.Set(bit);
			break;
		case SMPR2: 
			_SMPR2.Set(bit);
			break;
		case JOFR1: 
			_JOFR1.Set(bit);
			break;
		case JOFR2: 
			_JOFR2.Set(bit);
			break;
		case JOFR3: 
			_JOFR3.Set(bit);
			break;
		case JOFR4: 
			_JOFR4.Set(bit);
			break;
		case HTR: 
			_HTR.Set(bit);
			break;
		case LTR: 
			_LTR.Set(bit);
			break;
		case SQR1: 
			_SQR1.Set(bit);
			break;
		case SQR2: 
			_SQR2.Set(bit);
			break;
		case SQR3: 
			_SQR3.Set(bit);
			break;
		case JSQR: 
			_JSQR.Set(bit);
			break;
		case JDR1: 
			_JDR1.Set(bit);
			break;
		case JDR2: 
			_JDR2.Set(bit);
			break;
		case JDR3: 
			_JDR3.Set(bit);
			break;
		case JDR4: 
			_JDR4.Set(bit);
			break;
		case DR: 
			_DR.Set(bit);
			break;
	};
};

void ADC::Reset(enum ADC::ADC_ reg, Register::bit bit)
{
	switch (reg)
	{
	case SR:
			_SR.Reset(bit);
			break;
		case CR1:
			_CR1.Reset(bit);
			break;
		case CR2:
			_CR2.Reset(bit);
			break;
		case SMPR1:
			_SMPR1.Reset(bit);
			break;
		case SMPR2: 
			_SMPR2.Reset(bit);
			break;
		case JOFR1: 
			_JOFR1.Reset(bit);
			break;
		case JOFR2: 
			_JOFR2.Reset(bit);
			break;
		case JOFR3: 
			_JOFR3.Reset(bit);
			break;
		case JOFR4: 
			_JOFR4.Reset(bit);
			break;
		case HTR: 
			_HTR.Reset(bit);
			break;
		case LTR: 
			_LTR.Reset(bit);
			break;
		case SQR1: 
			_SQR1.Reset(bit);
			break;
		case SQR2: 
			_SQR2.Reset(bit);
			break;
		case SQR3: 
			_SQR3.Reset(bit);
			break;
		case JSQR: 
			_JSQR.Reset(bit);
			break;
		case JDR1: 
			_JDR1.Reset(bit);
			break;
		case JDR2: 
			_JDR2.Reset(bit);
			break;
		case JDR3: 
			_JDR3.Reset(bit);
			break;
		case JDR4: 
			_JDR4.Reset(bit);
			break;
		case DR: 
			_DR.Reset(bit);
			break;
	};
};




uint32_t ADC::Read(enum ADC::ADC_ reg)
{
	uint32_t value = 0;
	switch (reg)
	{
	case SR:
			 value = _SR.Read();
			break;
		case CR1:
			 value = _CR1.Read();
			break;
		case CR2:
			 value = _CR2.Read();
			break;
		case SMPR1:
			 value = _SMPR1.Read();
			break;
		case SMPR2: 
			 value = _SMPR2.Read();
			break;
		case JOFR1: 
			 value = _JOFR1.Read();
			break;
		case JOFR2: 
			 value = _JOFR2.Read();
			break;
		case JOFR3: 
			 value = _JOFR3.Read();
			break;
		case JOFR4: 
			 value = _JOFR4.Read();
			break;
		case HTR: 
			 value = _HTR.Read();
			break;
		case LTR: 
			 value = _LTR.Read();
			break;
		case SQR1: 
			 value = _SQR1.Read();
			break;
		case SQR2: 
			 value = _SQR2.Read();
			break;
		case SQR3: 
			 value = _SQR3.Read();
			break;
		case JSQR: 
			 value = _JSQR.Read();
			break;
		case JDR1: 
			 value = _JDR1.Read();
			break;
		case JDR2: 
			 value = _JDR2.Read();
			break;
		case JDR3: 
			 value = _JDR3.Read();
			break;
		case JDR4: 
			 value = _JDR4.Read();
			break;
		case DR: 
			 value = _DR.Read();
			break;
	};
	return value;
};

void ADC::Write(enum ADC::ADC_ reg, uint32_t value)
{
	switch (reg)
	{
	case SR:
			_SR.Write(value);
			break;
		case CR1:
			_CR1.Write(value);
			break;
		case CR2:
			_CR2.Write(value);
			break;
		case SMPR1:
			_SMPR1.Write(value);
			break;
		case SMPR2: 
			_SMPR2.Write(value);
			break;
		case JOFR1: 
			_JOFR1.Write(value);
			break;
		case JOFR2: 
			_JOFR2.Write(value);
			break;
		case JOFR3: 
			_JOFR3.Write(value);
			break;
		case JOFR4: 
			_JOFR4.Write(value);
			break;
		case HTR: 
			_HTR.Write(value);
			break;
		case LTR: 
			_LTR.Write(value);
			break;
		case SQR1: 
			_SQR1.Write(value);
			break;
		case SQR2: 
			_SQR2.Write(value);
			break;
		case SQR3: 
			_SQR3.Write(value);
			break;
		case JSQR: 
			_JSQR.Write(value);
			break;
		case JDR1: 
			_JDR1.Write(value);
			break;
		case JDR2: 
			_JDR2.Write(value);
			break;
		case JDR3: 
			_JDR3.Write(value);
			break;
		case JDR4: 
			_JDR4.Write(value);
			break;
		case DR: 
			_DR.Write(value);
			break;
	};
};
