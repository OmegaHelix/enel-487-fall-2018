#include "USART.h"
#include "GPIO.h"
#include "RCC.h"


class USART_Driver
{
	private:
		// USART register for the driver to utilize
		USART usart;
	public:
		
		//Constructor and Destructor
		USART_Driver(uint32_t);
		~USART_Driver();
		//Driver functions
		void open();
		void close();
		void start();
		void stop();
		
		//Useage Functions
		void SendByte(uint8_t);
		uint8_t GetByte();
		
		// constant for USART2
		static const uint32_t _USART2;
};

