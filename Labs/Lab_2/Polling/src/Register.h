#ifndef __REG
#define __REG
/******************************************************************************
* File: Register.h
* 
* Description: This file holds the basic reigster class, a register should be
*				written to and read. Other classes can expand upon this class.
*
* Member Variables:
*			reg:
*				a constant reference to a volatile unsigned integer
*
* Member Functions:
*			Register(uint32_t):
*				This function Creates an instance of a register from the given
*				unisgned integer as the address.
*
*			void Or(uint32_t Value);
*				This function takes in a value to Or into the register.
*
*			void And(uint32_t Value);
*				This function takes in a value to And with the register.
*
*			uint32_t Read(void);
*				This function returns the value of the register.
*
*
*
*
******************************************************************************/
#include "stdlib.h"
#include "stdint.h"

class Register
{
	private: 
	// reference to a volatile UInt
	// Can be referenced as register to be written to.
		uint32_t volatile & reg;
	public:
		Register(uint32_t);
		~Register();
		void Or(uint32_t);
		void And(uint32_t);
		uint32_t Read(void);
		void Set(uint32_t);
};

#endif


