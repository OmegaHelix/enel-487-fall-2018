#include "CLI_Driver.h"
void delay(int a);
 
int main(void)
{
        //Wait for the clock to stabilize. 
	while(!(RCC::Read(RCC::CR) & RCC::_CR_HSERDY)){};
        
		CLI_Driver CLI;
		CLI.open();
		CLI.start();
		CLI.CLI();
}
 
void delay (int a)
{
 volatile int i,j;
 for (i=0 ; i < a ; i++)
 {
  j++;
 }
 return;
}
