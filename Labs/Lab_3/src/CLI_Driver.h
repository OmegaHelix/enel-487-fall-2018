#include "USART_Driver.h"
#include "LED_Driver.h"

class CLI_Driver
{
	private:
			// create a usart driver to manipulate for USART2
			USART_Driver usart2; 
			uint8_t * buffer;
	public:
		//Constructor and destructor
		CLI_Driver();
		~CLI_Driver();

		//Driver functions
		void open();
		void close();
		void start();
		void stop();
	
		//Useage functions
		void getLine();
		void sendLine(uint8_t*);
		void sendMenu();
		void newLine();
		void InterperetBuffer();
		void printDoubleCarrot();
		bool bufferComp(uint8_t*);
		void sendHelp();
		void bufferClear();
		void CLI();

		void executeCommand(uint8_t*);
	
		static const int32_t BUF_MAX;
		static const uint8_t ENTER;
		static const uint8_t BKSP;

	
};

