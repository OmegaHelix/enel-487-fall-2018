#include "USART.h"
#include "GPIO.h"
#include "RCC.h"


class USART_Driver
{
	private:
		USART usart;
	public:
		USART_Driver(uint32_t);
		~USART_Driver();
		void SendByte(uint8_t);
		uint8_t GetByte();
		void open();
		void close();
		void start();
		void stop();
};

