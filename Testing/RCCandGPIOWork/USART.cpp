#include "USART.h"

USART::USART(uint32_t _BASE): _SR  (_BASE + 0x00),
															_DR  (_BASE + 0x04),
															_BRR (_BASE + 0x08),
															_CR1 (_BASE + 0x0C),
															_CR2 (_BASE + 0x10),
															_CR3 (_BASE + 0x14),
															_GTPR(_BASE + 0x18){};
USART::~USART(){};

	
void USART::Or(enum USART::USART_ reg, uint32_t value)
{
	switch (reg)
	{
		case SR:
			_SR.Or(value);
			break;
		case DR:
			_DR.Or(value);
			break;
		case BRR:
			_BRR.Or(value);
			break;
		case CR1:
			_CR1.Or(value);
			break;
		case CR2: 
			_CR2.Or(value);
			break;
		case CR3: 
			_CR3.Or(value);
			break;
		case GTPR: 
			_GTPR.Or(value);
			break;
	};
};
void USART::And(enum USART::USART_ reg, uint32_t value)
{
	switch (reg)
	{
		case SR:
			_SR.And(value);
			break;
		case DR:
			_DR.And(value);
			break;
		case BRR:
			_BRR.And(value);
			break;
		case CR1:
			_CR1.And(value);
			break;
		case CR2: 
			_CR2.And(value);
			break;
		case CR3: 
			_CR3.And(value);
			break;
		case GTPR: 
			_GTPR.And(value);
			break;
	};
};

void USART::Set(enum USART::USART_ reg, uint32_t value)
{
	switch (reg)
	{
		case SR:
			_SR.Set(value);
			break;
		case DR:
			_DR.Set(value);
			break;
		case BRR:
			_BRR.Set(value);
			break;
		case CR1:
			_CR1.Set(value);
			break;
		case CR2: 
			_CR2.Set(value);
			break;
		case CR3: 
			_CR3.Set(value);
			break;
		case GTPR: 
			_GTPR.Set(value);
			break;
	};
};
uint32_t USART::Read(enum USART::USART_ reg)
{
	uint32_t value = 0;
	switch (reg)
	{
		case SR:
			value = _SR.Read();
			break;
		case DR:
			value = _DR.Read();
			break;
		case BRR:
			value = _BRR.Read();
			break;
		case CR1:
			value = _CR1.Read();
			break;
		case CR2: 
			value = _CR2.Read();
			break;
		case CR3: 
			value = _CR3.Read();
			break;
		case GTPR: 
			value = _GTPR.Read();
			break;
	};
	return value;
};
