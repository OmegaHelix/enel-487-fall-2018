#include "USART_Driver.h"

USART_Driver::USART_Driver(uint32_t _BASE): usart(_BASE){}
USART_Driver::~USART_Driver(){}

void USART_Driver::SendByte(uint8_t byte)
{
	while(!usart.Read(USART::SR) & (0x1 << 7));
	
	usart.Set(USART::SR, byte);
}

uint8_t USART_Driver::GetByte()
{
	while(!usart.Read(USART::SR) & (0x1 << 5));
	
	return static_cast<uint8_t>(usart.Read(USART::SR));
}

void USART_Driver::open()
{
	// 1. Enable RCC GPIOA
	RCC::Or(RCC::APB2ENR, RCC::_APB2ENR_IOPAEN);

	// 2. Configure Tx and Rx GPIO pins
	//Rx Pin
	//AF out Push pull 10 11
	//Clear GPIOA port 3 configuration
	GPIO::And(GPIO::A, GPIO_PORT::CRL, ~(0xF << 12));
	//Set GPIOA port 3 configuration
	GPIO::Or(GPIO::A, GPIO_PORT::CRL, 0x4 << 12);
	
	//Tx Pin
	//floating input 01 00
	//Clear GPIOA port 2 configuration
	GPIO::And(GPIO::A, GPIO_PORT::CRL, ~(0xF << 8));
	//Set GPIOA port 2 configuration
	GPIO::Or(GPIO::A, GPIO_PORT::CRL, 0xB << 8);
	
	//3. Enable USART2 Clock
	RCC::Or(RCC::APB1ENR, RCC::_APB1ENR_USART2EN);
	
	//4. Set Baud Rate to 9600 0xEA6
	//USARTDIV = f(ck)/baud
	//USARTDIV = 36M/9600 = 0xEA6
	usart.Set(USART::BRR, 0xEA6);
	
	//5. Enable Tx/Rx
	// enable Tx
	usart.Or(USART::CR1, 0x1 << 3);
	
	// enable Rx
	usart.Or(USART::CR1, 0x1 << 2);
	
	// reset values set 
	// 8 bits data, no parity, and 1 stop bit.
	
}

void USART_Driver::close()
{
	// undo whatever open did
	// 1. Disable RCC GPIOA
	RCC::And(RCC::APB2ENR, ~RCC::_APB2ENR_IOPAEN);

	// 2. Configure Tx and Rx GPIO pins
	//Rx Pin
	//AF out Push pull 10 11
	//Clear GPIOA port 3 configuration
	GPIO::Or(GPIO::A, GPIO_PORT::CRL, ~(0xF << 12));
	//Set GPIOA port 3 configuration
	GPIO::And(GPIO::A, GPIO_PORT::CRL, ~(0x4 << 12));
	
	//Tx Pin
	//floating input 01 00
	//Clear GPIOA port 2 configuration
	GPIO::And(GPIO::A, GPIO_PORT::CRL, ~(0xF << 8));
	//Set GPIOA port 2 configuration
	GPIO::Or(GPIO::A, GPIO_PORT::CRL, 0xB << 8);
	
	//3. Enable USART2 Clock
	RCC::Or(RCC::APB1ENR, RCC::_APB1ENR_USART2EN);
	
	//4. Set Baud Rate to 9600 0xEA6
	//USARTDIV = f(ck)/baud
	//USARTDIV = 36M/9600 = 0xEA6
	usart.Set(USART::BRR, 0xEA6);
	
	//5. Enable Tx/Rx
	// enable Tx
	usart.Or(USART::CR1, 0x1 << 3);
	
	// enable Rx
	usart.Or(USART::CR1, 0x1 << 2);
	
	// reset values set 
	// 8 bits data, no parity, and 1 stop bit.
}

void USART_Driver::start()
{
	usart.Or(USART::CR1, 0x1 << 13); // set the Usart enable bit
}

void USART_Driver::stop()
{
	usart.And(USART::CR1, ~(0x1  << 13)); //clear the USART enable bit.
}



