#include "USART_Driver.h"
void delay(int a);
 
int main(void)
{
		// connect driver to usart2
		USART_Driver usart2_driver(0x40004400);

		// open the usart2 device
		usart2_driver.open();
		
		// start the usart2 device
		usart2_driver.start();
	
	while(1)
	{
		usart2_driver.SendByte('A');
		delay(5000000);
	}
	
	// turn off the usart to save power
	usart2_driver.stop();
	
	//close the driver once we're done
	usart2_driver.close();
	
}
 
void delay (int a)
{
 volatile int i,j;
 for (i=0 ; i < a ; i++)
 {
  j++;
 }
 return;
}
