#ifndef __RCC
#define __RCC

/******************************************************************************
* File: RCC.h
*
* Description: This file holds the basic RCC Register structure
*
* Member Variables:
*			reg:
*				a constant reference to a volatile unsigned integer
*
* Member Functions:
*			RCC(uint32_t):
*				This function Creates an instance of a RCC 
*
*
*			~RCC();
*			void Or(enum RCC::RCC_, uint32_t);
*			void And(enum RCC::RCC_, uint32_t);
*			void Set(enum RCC::RCC_, uint32_t);
*			uint32_t Read(enum RCC::RCC_);
*			enum RCC_ 
*				Contains the following enumerated values to allow for easier
*				reuse of functions and readability. To be used in function
*				calls such as Reg_RCC.Or(RCC::CR, 0x1);
*
*				CR, CFGR, CIR, APB2RSTR, APB1RSTR, 
*				AHBENR, APB2ENR, APB1ENR, BDCR.
*
*
*
*
*
******************************************************************************/
#include "Register.h"
class RCC
{
	private:
		//RCC's base adddress, no real way to avoid magic numbers here.
	static const uint32_t _BASE = 0x40021000;
	static Register _CR;
	static Register _CFGR;
	static Register _CIR;
	static Register _APB2RSTR;
	static Register _APB1RSTR;
	static Register _AHBENR;
	static Register _APB2ENR;
	static Register _APB1ENR;
	static Register _BDCR;
	static Register _CSR;

public:
	enum RCC_ {CR,CFGR,CIR,APB2RSTR,APB1RSTR,AHBENR,APB2ENR,APB1ENR,BDCR};
	RCC();
	~RCC();
	static void Or(enum RCC::RCC_, uint32_t);
	static void And(enum RCC::RCC_, uint32_t);
	static void Set(enum RCC::RCC_, uint32_t);
	static uint32_t Read(enum RCC_);


	// Library of useful configurations for RCC2 register sections.

	// Bit masks for RCC2_CR
	// Bits 30 and 31 are reserved.
	static const uint32_t _CR_PLL3RDY;
	static const uint32_t _CR_PLL3ON; 
	static const uint32_t _CR_PLL2RDY;
	static const uint32_t _CR_PLL2ON; 
	static const uint32_t _CR_CSSON;  
	static const uint32_t _CR_HSEBYP; 
	static const uint32_t _CR_HSERDY;
	static const uint32_t _CR_HSEON; 
	static const uint32_t _CR_HSICAL; 
	static const uint32_t _CR_HSITRIM; 
	static const uint32_t _CR_HSRDY;   
	static const uint32_t _CR_HSION;

	// Individual Bits for RCC2_CR mult i-bit Registers
	// HSICAL is initialized automatically on setup.
	// These can be used for reading only. 
	static const uint32_t _CR_HSICAL_0;
	static const uint32_t _CR_HSICAL_1;
	static const uint32_t _CR_HSICAL_2; 
	static const uint32_t _CR_HSICAL_3;
	static const uint32_t _CR_HSICAL_4; 
	static const uint32_t _CR_HSICAL_5; 
	static const uint32_t _CR_HSICAL_6; 
	static const uint32_t _CR_HSICAL_7; 
	// HSITRIM can be programmed. 
	// These can be used for Read and Write.
	static const uint32_t _CR_HSITRIM_0; 
	static const uint32_t _CR_HSITRIM_1; 
	static const uint32_t _CR_HSITRIM_2; 
	static const uint32_t _CR_HSITRIM_3;
	static const uint32_t _CR_HSITRIM_4;
	
	// Bit Masks for RCC2_CFGR Register
	// Bits 23, 27-31 are reserved.
	static const uint32_t _CFGR_SW;
	static const uint32_t _CFGR_SWS;
	static const uint32_t _CFGR_HPRE;
	static const uint32_t _CFGR_PPRE1;
	static const uint32_t _CFGR_PPRE2;
	static const uint32_t _CFGR_ADCPRE;
	static const uint32_t _CFGR_PLLSRC;
	static const uint32_t _CFGR_PLLXTPRE;
	static const uint32_t _CFGR_PLLMUL;
	static const uint32_t _CFGR_USBPRE;
	static const uint32_t _CFGR_MCO;
	
	
	//Individual bits for multibit sections
	static const uint32_t _CFGR_SW_0;	
	static const uint32_t _CFGR_SW_1;
	
	// SWS is read only
	static const uint32_t _CFGR_SWS_0;
	static const uint32_t _CFGR_SWS_1;
	
	static const uint32_t _CFGR_HPRE_0;
	static const uint32_t _CFGR_HPRE_1;
	static const uint32_t _CFGR_HPRE_2;
	static const uint32_t _CFGR_HPRE_3;
	
	static const uint32_t _CFGR_PPRE1_0;
	static const uint32_t _CFGR_PPRE1_1;
	static const uint32_t _CFGR_PPRE1_2;
	
	static const uint32_t _CFGR_PPRE2_0;
	static const uint32_t _CFGR_PPRE2_1;
	static const uint32_t _CFGR_PPRE2_2;
	
	static const uint32_t _CFGR_ADCPRE_0;
	static const uint32_t _CFGR_ADCPRE_1;
	
	static const uint32_t _CFGR_PLLMUL_0;
	static const uint32_t _CFGR_PLLMUL_1;
	static const uint32_t _CFGR_PLLMUL_2;
	static const uint32_t _CFGR_PLLMUL_3;
	
	static const uint32_t _CFGR_MCO_0;
	static const uint32_t _CFGR_MCO_1;
	static const uint32_t _CFGR_MCO_2;
	
	
	// Bits for the CIR Clock Interrupt Register
	// Bits 5,6,13,14,15,21,22, and 24-31 are reserved.
	static const uint32_t _CIR_LSIRDYF;
	static const uint32_t _CIR_LSERDYF;
	static const uint32_t _CIR_HSIRDYF;
	static const uint32_t _CIR_HSERDYF;
	static const uint32_t _CIR_PLLRDYF;
	static const uint32_t _CIR_CSSF;
	static const uint32_t _CIR_LSIRDYIE;
	static const uint32_t _CIR_LSERDYIE;
	static const uint32_t _CIR_HSIRDYIE;
	static const uint32_t _CIR_HSERDYIE;
	static const uint32_t _CIR_PLLRDYIE;
	static const uint32_t _CIR_LSIRDYC;
	static const uint32_t _CIR_LSERDYC;
	static const uint32_t _CIR_HSIRDYC;
	static const uint32_t _CIR_HSERDYC;
	static const uint32_t _CIR_PLLRDYC;
	static const uint32_t _CIR_CSSC;
	
	// Bits for the APB2RSTR peripheral reset register
	// Bits 16-18, and 22-31 are reserved
	static const uint32_t _APB2RSTR_AFIORST;
	static const uint32_t _APB2RSTR_IOPARST;
	static const uint32_t _APB2RSTR_IOPBRST;
	static const uint32_t _APB2RSTR_IOPCRST;
	static const uint32_t _APB2RSTR_IOPDRST;
	static const uint32_t _APB2RSTR_IOPERST;
	static const uint32_t _APB2RSTR_IOPFRST;
	static const uint32_t _APB2RSTR_IOPGRST;
	static const uint32_t _APB2RSTR_ADC1RST;
	static const uint32_t _APB2RSTR_ADC2RST;
	static const uint32_t _APB2RSTR_TIM1RST;
	static const uint32_t _APB2RSTR_SPI1RST;
	static const uint32_t _APB2RSTR_TIM8RST;
	static const uint32_t _APB2RSTR_USART1RST;
	static const uint32_t _APB2RSTR_ADC3RST;
	static const uint32_t _APB2RSTR_TIM9RST;
	static const uint32_t _APB2RSTR_TIM10RST;
	static const uint32_t _APB2RSTR_TIM11RST;
	
	
	//Bits for the APB1RSTR peripheral reset register
	// Bits 9,10, 12, 13, 16, 24, 26, 30 and 31 are reserved
	//All Bits are read/write
	static const uint32_t _APB1RSTR_TIM2RST;
	static const uint32_t _APB1RSTR_TIM3RST;
	static const uint32_t _APB1RSTR_TIM4RST;
	static const uint32_t _APB1RSTR_TIM5RST;
	static const uint32_t _APB1RSTR_TIM6RST;
	static const uint32_t _APB1RSTR_TIM7RST;
	static const uint32_t _APB1RSTR_TIM12RST;
	static const uint32_t _APB1RSTR_TIM13RST;
	static const uint32_t _APB1RSTR_TIM14RST;
	static const uint32_t _APB1RSTR_WWDGRST;
	static const uint32_t _APB1RSTR_SPI2RST;
	static const uint32_t _APB1RSTR_SPI3RST;
	static const uint32_t _APB1RSTR_USART2RST;
	static const uint32_t _APB1RSTR_USART3RST;
	static const uint32_t _APB1RSTR_UART4RST;
	static const uint32_t _APB1RSTR_UART5RST;
	static const uint32_t _APB1RSTR_I2C1RST;
	static const uint32_t _APB1RSTR_I2C2RST;
	static const uint32_t _APB1RSTR_USBRST;
	static const uint32_t _APB1RSTR_CANRST;
	static const uint32_t _APB1RSTR_BKPRST;
	static const uint32_t _APB1RSTR_PWRRST;
	static const uint32_t _APB1RSTR_DACRST;
	
	//Bits for the AHBENR peripheral clock enable register
	//Bits 3, 4, 7, 9, 11-31 are reserved
	//All bits are read/write
	static const uint32_t _AHBENR_DMA1EN;
	static const uint32_t _AHBENR_DMA2EN;
	static const uint32_t _AHBENR_SRAMEN;
	static const uint32_t _AHBENR_FLITFEN;
	static const uint32_t _AHBENR_CRCEN;
	static const uint32_t _AHBENR_FSMCEN;
	static const uint32_t _AHBENR_SDIOEN;
	
	
	// Bits for the APB2ENR peripheral reset register
	// Bits 16-18, and 22-31 are reserved
	static const uint32_t _APB2ENR_AFIOEN;
	static const uint32_t _APB2ENR_IOPAEN;
	static const uint32_t _APB2ENR_IOPBEN;
	static const uint32_t _APB2ENR_IOPCEN;
	static const uint32_t _APB2ENR_IOPDEN;
	static const uint32_t _APB2ENR_IOPEEN;
	static const uint32_t _APB2ENR_IOPFEN;
	static const uint32_t _APB2ENR_IOPGEN;
	static const uint32_t _APB2ENR_ADC1EN;
	static const uint32_t _APB2ENR_ADC2EN;
	static const uint32_t _APB2ENR_TIM1EN;
	static const uint32_t _APB2ENR_SPI1EN;
	static const uint32_t _APB2ENR_TIM8EN;
	static const uint32_t _APB2ENR_USART1EN;
	static const uint32_t _APB2ENR_ADC3EN;
	static const uint32_t _APB2ENR_TIM9EN;
	static const uint32_t _APB2ENR_TIM10EN;
	static const uint32_t _APB2ENR_TIM11EN;
	
	
	//Bits for the APB1ENR peripheral reset register
	// Bits 9,10, 12, 13, 16, 24, 26, 30 and 31 are reserved
	//All Bits are read/write
	static const uint32_t _APB1ENR_TIM2EN;
	static const uint32_t _APB1ENR_TIM3EN;
	static const uint32_t _APB1ENR_TIM4EN;
	static const uint32_t _APB1ENR_TIM5EN;
	static const uint32_t _APB1ENR_TIM6EN;
	static const uint32_t _APB1ENR_TIM7EN;
	static const uint32_t _APB1ENR_TIM12EN;
	static const uint32_t _APB1ENR_TIM13EN;
	static const uint32_t _APB1ENR_TIM14EN;
	static const uint32_t _APB1ENR_WWDGEN;
	static const uint32_t _APB1ENR_SPI2EN;
	static const uint32_t _APB1ENR_SPI3EN;
	static const uint32_t _APB1ENR_USART2EN;
	static const uint32_t _APB1ENR_USART3EN;
	static const uint32_t _APB1ENR_UART4EN;
	static const uint32_t _APB1ENR_UART5EN;
	static const uint32_t _APB1ENR_I2C1EN;
	static const uint32_t _APB1ENR_I2C2EN;
	static const uint32_t _APB1ENR_USBEN;
	static const uint32_t _APB1ENR_CANEN;
	static const uint32_t _APB1ENR_BKPEN;
	static const uint32_t _APB1ENR_PWREN;
	static const uint32_t _APB1ENR_DACEN;
	
	//Bits for the BDCR backup domain control register
	// Bits 3-7, 10-14, and 17-31 a reserved.
	//read only
	static const uint32_t _BDCR_LSERDY;
	//read and write
	static const uint32_t _BDCR_LSEON;
	static const uint32_t _BDCR_LSEBYP;
	static const uint32_t _BDCR_RTCSEL;
	static const uint32_t _BDCR_RTCEN;
	static const uint32_t _BDCR_BDRST;
	
	//Individual bits for multibit sections
	//Read and write
	static const uint32_t _BDCR_RTCSEL_0;
	static const uint32_t _BDCR_RTCSEL_1;
	
	//Bits for the CSR Control/Status Register
	//Bits 2-23 and 25 are reserved.
	//read only
	static const uint32_t _CSR_LSIRDY;
	//Read Write
	static const uint32_t _CSR_LSION;
	static const uint32_t _CSR_RMVF;
	static const uint32_t _CSR_PINRSTF;
	static const uint32_t _CSR_PORRSTF;
	static const uint32_t _CSR_STFRSTF;
	static const uint32_t _CSR_IWDGRSTF;
	static const uint32_t _CSR_WWDFRSTF;
	static const uint32_t _CSR_LPWRRSTF;
	
	
};

#endif
