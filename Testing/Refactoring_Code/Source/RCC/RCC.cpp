#include "RCC.h"
// Library of useful configurations for RCC register sections.

// Bit masks for RCC_CR
// Bits 30 and 31 are reserved.
const uint32_t RCC::_CR_PLL3RDY = 0x1u << 29;
const uint32_t RCC::_CR_PLL3ON  = 0x1u << 28;
const uint32_t RCC::_CR_PLL2RDY = 0x1u << 27;
const uint32_t RCC::_CR_PLL2ON  = 0x1u << 26;
const uint32_t RCC::_CR_CSSON   = 0x1u << 19;
const uint32_t RCC::_CR_HSEBYP  = 0x1u << 18;
const uint32_t RCC::_CR_HSERDY  = 0x1u << 17;
const uint32_t RCC::_CR_HSEON   = 0x1u << 16;
const uint32_t RCC::_CR_HSICAL  = 0xFFu << 8;
const uint32_t RCC::_CR_HSITRIM = 0x1Fu << 3;
const uint32_t RCC::_CR_HSRDY   = 0x1u << 1;
const uint32_t RCC::_CR_HSION   = 0x1u << 0;

// Individual Bits for RCC_CR multi-bit Registers
// HSICAL is initialized automatically on setup.
// These can be used for reading only. 
const uint32_t RCC::_CR_HSICAL_0 = 0x1u << 8;
const uint32_t RCC::_CR_HSICAL_1 = 0x1u << 9;
const uint32_t RCC::_CR_HSICAL_2 = 0x1u << 10;
const uint32_t RCC::_CR_HSICAL_3 = 0x1u << 11;
const uint32_t RCC::_CR_HSICAL_4 = 0x1u << 12;
const uint32_t RCC::_CR_HSICAL_5 = 0x1u << 13;
const uint32_t RCC::_CR_HSICAL_6 = 0x1u << 14;
const uint32_t RCC::_CR_HSICAL_7 = 0x1u << 15;
// HSITRIM can be programmed. 
// These can be used for Read and Write.
const uint32_t RCC::_CR_HSITRIM_0 = 0x1u << 3;
const uint32_t RCC::_CR_HSITRIM_1 = 0x1u << 4;
const uint32_t RCC::_CR_HSITRIM_2 = 0x1u << 5;
const uint32_t RCC::_CR_HSITRIM_3 = 0x1u << 6;
const uint32_t RCC::_CR_HSITRIM_4 = 0x1u << 7; 

// Bit Masks for RCC_CFGR Register
// Bits 23, 27-31 are reserved.
const uint32_t RCC::_CFGR_SW       = 0x3u;
const uint32_t RCC::_CFGR_SWS      = 0x3u << 2;
const uint32_t RCC::_CFGR_HPRE     = 0xFu << 4;
const uint32_t RCC::_CFGR_PPRE1    = 0x7u << 8;
const uint32_t RCC::_CFGR_PPRE2    = 0x7u << 11;
const uint32_t RCC::_CFGR_ADCPRE   = 0x3u << 14;
const uint32_t RCC::_CFGR_PLLSRC   = 0x1u << 16;
const uint32_t RCC::_CFGR_PLLXTPRE = 0x1u << 17;
const uint32_t RCC::_CFGR_PLLMUL   = 0xFu << 18;
const uint32_t RCC::_CFGR_USBPRE   = 0x1u << 22;
const uint32_t RCC::_CFGR_MCO      = 0x7u << 24;


//Individual bits for multibit sections
const uint32_t RCC::_CFGR_SW_0 = 0x1u;	
const uint32_t RCC::_CFGR_SW_1 = 0x1u << 1;

// SWS is read only
const uint32_t RCC::_CFGR_SWS_0 = 0x1u << 2;
const uint32_t RCC::_CFGR_SWS_1 = 0x1u << 3;

const uint32_t RCC::_CFGR_HPRE_0 = 0x1u << 4;
const uint32_t RCC::_CFGR_HPRE_1 = 0x1u << 5;
const uint32_t RCC::_CFGR_HPRE_2 = 0x1u << 6;
const uint32_t RCC::_CFGR_HPRE_3 = 0x1u << 7;

const uint32_t RCC::_CFGR_PPRE1_0 = 0x1u << 8;
const uint32_t RCC::_CFGR_PPRE1_1 = 0x1u << 9;
const uint32_t RCC::_CFGR_PPRE1_2 = 0x1u << 10;

const uint32_t RCC::_CFGR_PPRE2_0 = 0x1u << 11;
const uint32_t RCC::_CFGR_PPRE2_1 = 0x1u << 12;
const uint32_t RCC::_CFGR_PPRE2_2 = 0x1u << 13;

const uint32_t RCC::_CFGR_ADCPRE_0 = 0x1u << 14;
const uint32_t RCC::_CFGR_ADCPRE_1 = 0x1u << 15;

const uint32_t RCC::_CFGR_PLLMUL_0 = 0x1u << 18;
const uint32_t RCC::_CFGR_PLLMUL_1 = 0x1u << 19;
const uint32_t RCC::_CFGR_PLLMUL_2 = 0x1u << 20;
const uint32_t RCC::_CFGR_PLLMUL_3 = 0x1u << 21;

const uint32_t RCC::_CFGR_MCO_0 = 0x1u << 24;
const uint32_t RCC::_CFGR_MCO_1 = 0x1u << 25;
const uint32_t RCC::_CFGR_MCO_2 = 0x1u << 26;


// Bits for the CIR Clock Interrupt Register
// Bits 5,6,13,14,15,21,22, and 24-31 are reserved.
//read only bits
const uint32_t RCC::_CIR_LSIRDYF = 0x1u;
const uint32_t RCC::_CIR_LSERDYF = 0x1u << 1;
const uint32_t RCC::_CIR_HSIRDYF = 0x1u << 2;
const uint32_t RCC::_CIR_HSERDYF = 0x1u << 3;
const uint32_t RCC::_CIR_PLLRDYF = 0x1u << 4;
const uint32_t RCC::_CIR_CSSF    = 0x1u << 7;

//read/write bits
const uint32_t RCC::_CIR_LSIRDYIE = 0x1u << 8;
const uint32_t RCC::_CIR_LSERDYIE = 0x1u << 9;
const uint32_t RCC::_CIR_HSIRDYIE = 0x1u << 10;
const uint32_t RCC::_CIR_HSERDYIE = 0x1u << 11;
const uint32_t RCC::_CIR_PLLRDYIE = 0x1u << 12;

//write only bits
const uint32_t RCC::_CIR_LSIRDYC = 0x1u << 16;
const uint32_t RCC::_CIR_LSERDYC = 0x1u << 17;
const uint32_t RCC::_CIR_HSIRDYC = 0x1u << 18;
const uint32_t RCC::_CIR_HSERDYC = 0x1u << 19;
const uint32_t RCC::_CIR_PLLRDYC = 0x1u << 20;
const uint32_t RCC::_CIR_CSSC    = 0x1u << 23;

// Bits for the APB2RSTR peripheral reset register
// Bits 16-18, and 22-31 are reserved
// All bits are read/write
const uint32_t RCC::_APB2RSTR_AFIORST   = 0x1u;
const uint32_t RCC::_APB2RSTR_IOPARST   = 0x1u << 2;
const uint32_t RCC::_APB2RSTR_IOPBRST   = 0x1u << 3;
const uint32_t RCC::_APB2RSTR_IOPCRST   = 0x1u << 4;
const uint32_t RCC::_APB2RSTR_IOPDRST   = 0x1u << 5;
const uint32_t RCC::_APB2RSTR_IOPERST   = 0x1u << 6;
const uint32_t RCC::_APB2RSTR_IOPFRST   = 0x1u << 7;
const uint32_t RCC::_APB2RSTR_IOPGRST   = 0x1u << 8;
const uint32_t RCC::_APB2RSTR_ADC1RST   = 0x1u << 9;
const uint32_t RCC::_APB2RSTR_ADC2RST   = 0x1u << 10;
const uint32_t RCC::_APB2RSTR_TIM1RST   = 0x1u << 11;
const uint32_t RCC::_APB2RSTR_SPI1RST   = 0x1u << 12;
const uint32_t RCC::_APB2RSTR_TIM8RST   = 0x1u << 13;
const uint32_t RCC::_APB2RSTR_USART1RST = 0x1u << 14;
const uint32_t RCC::_APB2RSTR_ADC3RST   = 0x1u << 15;
const uint32_t RCC::_APB2RSTR_TIM9RST   = 0x1u << 19;
const uint32_t RCC::_APB2RSTR_TIM10RST  = 0x1u << 20;
const uint32_t RCC::_APB2RSTR_TIM11RST  = 0x1u << 21;

//Bits for the APB1RSTR peripheral reset register
// Bits 9,10, 12, 13, 16, 24, 26, 30 and 31 are reserved
// All bits are read/write
const uint32_t RCC::_APB1RSTR_TIM2RST   = 0x1u;
const uint32_t RCC::_APB1RSTR_TIM3RST   = 0x1u << 1;
const uint32_t RCC::_APB1RSTR_TIM4RST   = 0x1u << 2;
const uint32_t RCC::_APB1RSTR_TIM5RST   = 0x1u << 3;
const uint32_t RCC::_APB1RSTR_TIM6RST   = 0x1u << 4;
const uint32_t RCC::_APB1RSTR_TIM7RST   = 0x1u << 5;
const uint32_t RCC::_APB1RSTR_TIM12RST  = 0x1u << 6;
const uint32_t RCC::_APB1RSTR_TIM13RST  = 0x1u << 7;
const uint32_t RCC::_APB1RSTR_TIM14RST  = 0x1u << 8;
const uint32_t RCC::_APB1RSTR_WWDGRST   = 0x1u << 11;
const uint32_t RCC::_APB1RSTR_SPI2RST   = 0x1u << 14;
const uint32_t RCC::_APB1RSTR_SPI3RST   = 0x1u << 15;
const uint32_t RCC::_APB1RSTR_USART2RST = 0x1u << 17;
const uint32_t RCC::_APB1RSTR_USART3RST = 0x1u << 18;
const uint32_t RCC::_APB1RSTR_UART4RST  = 0x1u << 19;
const uint32_t RCC::_APB1RSTR_UART5RST  = 0x1u << 20;
const uint32_t RCC::_APB1RSTR_I2C1RST   = 0x1u << 21;
const uint32_t RCC::_APB1RSTR_I2C2RST   = 0x1u << 22;
const uint32_t RCC::_APB1RSTR_USBRST    = 0x1u << 23;
const uint32_t RCC::_APB1RSTR_CANRST    = 0x1u << 25;
const uint32_t RCC::_APB1RSTR_BKPRST    = 0x1u << 27;
const uint32_t RCC::_APB1RSTR_PWRRST    = 0x1u << 28;
const uint32_t RCC::_APB1RSTR_DACRST    = 0x1u << 29;

//Bits for the AHBENR peripheral clock enable register
//Bits 3, 4, 7, 9, 11-31 are reserved
//All bits are read/write
const uint32_t RCC::_AHBENR_DMA1EN  = 0x1u;
const uint32_t RCC::_AHBENR_DMA2EN  = 0x1u << 1;
const uint32_t RCC::_AHBENR_SRAMEN  = 0x1u << 2;
const uint32_t RCC::_AHBENR_FLITFEN = 0x1u << 4;
const uint32_t RCC::_AHBENR_CRCEN   = 0x1u << 6;
const uint32_t RCC::_AHBENR_FSMCEN  = 0x1u << 8;
const uint32_t RCC::_AHBENR_SDIOEN  = 0x1u << 10;


// Bits for the APB2ENR peripheral reset register
// Bits 16-18, and 22-31 are reserved
// All bits are read/write
 const uint32_t RCC::_APB2ENR_AFIOEN   = 0x1u;
 const uint32_t RCC::_APB2ENR_IOPAEN   = 0x1u << 2;
 const uint32_t RCC::_APB2ENR_IOPBEN   = 0x1u << 3;
 const uint32_t RCC::_APB2ENR_IOPCEN   = 0x1u << 4;
 const uint32_t RCC::_APB2ENR_IOPDEN   = 0x1u << 5;
 const uint32_t RCC::_APB2ENR_IOPEEN   = 0x1u << 6;
 const uint32_t RCC::_APB2ENR_IOPFEN   = 0x1u << 7;
 const uint32_t RCC::_APB2ENR_IOPGEN   = 0x1u << 8;
 const uint32_t RCC::_APB2ENR_ADC1EN   = 0x1u << 9;
 const uint32_t RCC::_APB2ENR_ADC2EN   = 0x1u << 10;
 const uint32_t RCC::_APB2ENR_TIM1EN   = 0x1u << 11;
 const uint32_t RCC::_APB2ENR_SPI1EN   = 0x1u << 12;
 const uint32_t RCC::_APB2ENR_TIM8EN   = 0x1u << 13;
 const uint32_t RCC::_APB2ENR_USART1EN = 0x1u << 14;
 const uint32_t RCC::_APB2ENR_ADC3EN   = 0x1u << 15;
 const uint32_t RCC::_APB2ENR_TIM9EN   = 0x1u << 19;
 const uint32_t RCC::_APB2ENR_TIM10EN  = 0x1u << 20;
 const uint32_t RCC::_APB2ENR_TIM11EN  = 0x1u << 21;

//Bits for the APB1ENR peripheral reset register
// Bits 9,10, 12, 13, 16, 24, 26, 30 and 31 are reserved
// All bits are read/write
const uint32_t RCC::_APB1ENR_TIM2EN   = 0x1u;
const uint32_t RCC::_APB1ENR_TIM3EN   = 0x1u << 1;
const uint32_t RCC::_APB1ENR_TIM4EN   = 0x1u << 2;
const uint32_t RCC::_APB1ENR_TIM5EN   = 0x1u << 3;
const uint32_t RCC::_APB1ENR_TIM6EN   = 0x1u << 4;
const uint32_t RCC::_APB1ENR_TIM7EN   = 0x1u << 5;
const uint32_t RCC::_APB1ENR_TIM12EN  = 0x1u << 6;
const uint32_t RCC::_APB1ENR_TIM13EN  = 0x1u << 7;
const uint32_t RCC::_APB1ENR_TIM14EN  = 0x1u << 8;
const uint32_t RCC::_APB1ENR_WWDGEN   = 0x1u << 11;
const uint32_t RCC::_APB1ENR_SPI2EN   = 0x1u << 14;
const uint32_t RCC::_APB1ENR_SPI3EN   = 0x1u << 15;
const uint32_t RCC::_APB1ENR_USART2EN = 0x1u << 17;
const uint32_t RCC::_APB1ENR_USART3EN = 0x1u << 18;
const uint32_t RCC::_APB1ENR_UART4EN  = 0x1u << 19;
const uint32_t RCC::_APB1ENR_UART5EN  = 0x1u << 20;
const uint32_t RCC::_APB1ENR_I2C1EN   = 0x1u << 21;
const uint32_t RCC::_APB1ENR_I2C2EN   = 0x1u << 22;
const uint32_t RCC::_APB1ENR_USBEN    = 0x1u << 23;
const uint32_t RCC::_APB1ENR_CANEN    = 0x1u << 25;
const uint32_t RCC::_APB1ENR_BKPEN    = 0x1u << 27;
const uint32_t RCC::_APB1ENR_PWREN    = 0x1u << 28;
const uint32_t RCC::_APB1ENR_DACEN    = 0x1u << 29;


//Bits for the BDCR backup domain control register
// Bits 3-7, 10-14, and 17-31 a reserved.
//read only
const uint32_t RCC::_BDCR_LSERDY = 0x1u << 1;
//read and write
const uint32_t RCC::_BDCR_LSEON = 0x1u;
const uint32_t RCC::_BDCR_LSEBYP = 0x1u << 2;
const uint32_t RCC::_BDCR_RTCSEL = 0x3u << 8;
const uint32_t RCC::_BDCR_RTCEN = 0x1u << 15;
const uint32_t RCC::_BDCR_BDRST = 0x1u << 16;

//Individual bits for multibit sections
//Read and write
const uint32_t RCC::_BDCR_RTCSEL_0 = 0x1u << 8;
const uint32_t RCC::_BDCR_RTCSEL_1 = 0x1u << 9;


//Bits for the CSR Control/Status Register
//Bits 2-23 and 25 are reserved.
//read only
const uint32_t RCC::_CSR_LSIRDY = 0x1u << 1;
//Read Write
const uint32_t RCC::_CSR_LSION = 0x1u;
const uint32_t RCC::_CSR_RMVF = 0x1u << 24;
const uint32_t RCC::_CSR_PINRSTF = 0x1u << 26;
const uint32_t RCC::_CSR_PORRSTF = 0x1u << 27;
const uint32_t RCC::_CSR_STFRSTF = 0x1u << 28;
const uint32_t RCC::_CSR_IWDGRSTF = 0x1u << 29;
const uint32_t RCC::_CSR_WWDFRSTF = 0x1u << 30;
const uint32_t RCC::_CSR_LPWRRSTF = 0x1u << 31;

Register RCC::_CR(RCC::_BASE + 0x00); 
Register RCC::_CFGR(RCC::_BASE + 0x04);
Register RCC::_CIR(_BASE + 0x08);
Register RCC::_APB2RSTR(RCC::_BASE + 0x0C);
Register RCC::_APB1RSTR(_BASE + 0x10);
Register RCC::_AHBENR(_BASE + 0x14);
Register RCC::_APB2ENR(_BASE + 0x18);
Register RCC::_APB1ENR(_BASE + 0x1C);
Register RCC::_BDCR(_BASE + 0x20);
Register RCC::_CSR(_BASE + 0x24);
 
RCC::RCC() {};


RCC::~RCC()
{
}

void RCC::Or(enum RCC::RCC_ reg, uint32_t value)
{
	switch (reg)
	{
		case CR:
			_CR.Or(value);
			break;
		case CFGR:
			_CFGR.Or(value);
			break;
		case CIR:
			_CIR.Or(value);
			break;
		case APB2RSTR:
			_APB2RSTR.Or(value);
			break;
		case APB1RSTR: 
			_APB1RSTR.Or(value);
			break;
		case AHBENR: 
			_AHBENR.Or(value);
			break;
		case APB2ENR: 
			_APB2ENR.Or(value);
			break;
		case APB1ENR:
			_APB1ENR.Or(value);
			break;
		case BDCR:
			_BDCR.Or(value);
			break;
	};
	

}

void RCC::And(enum RCC::RCC_ reg, uint32_t value)
{
	switch (reg)
	{
	case CR:
		_CR.And(value);
		break;
	case CFGR:
		_CFGR.And(value);
		break;
	case CIR:
		_CIR.And(value);
		break;
	case APB2RSTR:
		_APB2RSTR.And(value);
		break;
	case APB1RSTR:
		_APB1RSTR.And(value);
		break;
	case AHBENR:
		_AHBENR.And(value);
		break;
	case APB2ENR:
		_APB2ENR.And(value);
		break;
	case APB1ENR:
		_APB1ENR.And(value);
		break;
	case BDCR:
		_BDCR.And(value);
		break;
	};


}

void RCC::Set(enum RCC::RCC_ reg, Register::bit bit)
{
	switch (reg)
	{
	case CR:
		_CR.Set(bit);
		break;
	case CFGR:
		_CFGR.Set(bit);
		break;
	case CIR:
		_CIR.Set(bit);
		break;
	case APB2RSTR:
		_APB2RSTR.Set(bit);
		break;
	case APB1RSTR:
		_APB1RSTR.Set(bit);
		break;
	case AHBENR:
		_AHBENR.Set(bit);
		break;
	case APB2ENR:
		_APB2ENR.Set(bit);
		break;
	case APB1ENR:
		_APB1ENR.Set(bit);
		break;
	case BDCR:
		_BDCR.Set(bit);
		break;
	};
}

void RCC::Reset(enum RCC::RCC_ reg, Register::bit bit)
{
	switch (reg)
	{
	case CR:
		_CR.Reset(bit);
		break;
	case CFGR:
		_CFGR.Reset(bit);
		break;
	case CIR:
		_CIR.Reset(bit);
		break;
	case APB2RSTR:
		_APB2RSTR.Reset(bit);
		break;
	case APB1RSTR:
		_APB1RSTR.Reset(bit);
		break;
	case AHBENR:
		_AHBENR.Reset(bit);
		break;
	case APB2ENR:
		_APB2ENR.Reset(bit);
		break;
	case APB1ENR:
		_APB1ENR.Reset(bit);
		break;
	case BDCR:
		_BDCR.Reset(bit);
		break;
	};
}



uint32_t RCC::Read(enum RCC_ reg)
{
	uint32_t value = 0;
	switch (reg)
	{
	case CR:
		value = _CR.Read();
		break;
	case CFGR:
		value = _CFGR.Read();
		break;
	case CIR:
		value = _CIR.Read();
		break;
	case APB2RSTR:
		value = _APB2RSTR.Read();
		break;
	case APB1RSTR:
		value = _APB1RSTR.Read();
		break;
	case AHBENR:
		value = _AHBENR.Read();
		break;
	case APB2ENR:
		value = _APB2ENR.Read();
		break;
	case APB1ENR:
		value = _APB1ENR.Read();
		break;
	case BDCR:
		value = _BDCR.Read();
		break;
	};
	
	return value;
}

void RCC::Write(enum RCC::RCC_ reg, uint32_t value)
{
	switch (reg)
	{
	case CR:
		_CR.Write(value);
		break;
	case CFGR:
		_CFGR.Write(value);
		break;
	case CIR:
		_CIR.Write(value);
		break;
	case APB2RSTR:
		_APB2RSTR.Write(value);
		break;
	case APB1RSTR:
		_APB1RSTR.Write(value);
		break;
	case AHBENR:
		_AHBENR.Write(value);
		break;
	case APB2ENR:
		_APB2ENR.Write(value);
		break;
	case APB1ENR:
		_APB1ENR.Write(value);
		break;
	case BDCR:
		_BDCR.Write(value);
		break;
	};


}


