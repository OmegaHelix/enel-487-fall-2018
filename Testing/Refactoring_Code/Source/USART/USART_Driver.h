#include "USART.h"
#include "GPIO.h"
#include "RCC.h"


class USART_Driver
{
	private:
		// USART register for the driver to utilize
		USART usart;
                static USART USART2;
                static uint8_t CHARBUF[];
	public:
		
		//Constructor and Destructor
		USART_Driver(uint32_t);
		~USART_Driver();
		//Driver functions
		void open();
		void close();
		void start();
		void stop();
		
		//Usage Functions
		void SendByte(uint8_t);
		uint8_t GetByte();
                static void Interrupt(void);
		
		// constant for USART2
		static const uint32_t _USART2;
};

//interrupt data

extern "C" void USART2_IRQHandler(void);





