#include "CLI_Driver.h"


	const int32_t CLI_Driver::BUF_MAX = 31;
	const uint8_t CLI_Driver::ENTER = 0xD;
	const uint8_t CLI_Driver::BKSP = 0x7F;

	static uint8_t HELP[] = "HELP";
	static uint8_t HELP_LINE[] = "The possible commands are:";
	static uint8_t HELP_DESC[] = "\tHELP provides the above list of command descriptions";
	static uint8_t LED_LINE[] = "LED <# 1 through 8 or ALL> <ON, OFF, or STATE>";
	static uint8_t LED_ON_DESC_1[] = "\tLED # ON\t provides power to the LED";
	static uint8_t LED_ON_DESC_2[] = "\tLED ALL ON\t provides power to all LED's";	
	static uint8_t LED_OFF_DESC_1[] = "\tLED # OFF\t turns off power to the LED";
	static uint8_t LED_OFF_DESC_2[] = "\tLED ALL OFF\t turns off power to all LED's";	
	static uint8_t LED_STATE_DESC_1[] = "\tLED # STATE\t provides power status of the LED";
	static uint8_t LED_STATE_DESC_2[] = "\tLED ALL STATE\t provides power status of all LED's";

	
	static uint8_t COMPILE_TIME[] = "COMPILE TIME";	
	static uint8_t COMPILE_TIME_DESC[] = "\tCOMPILE TIME displays the date and time the source file was compiled";
	static uint8_t COMPILE_LINE[] = "Compiled at: " __DATE__ " " __TIME__;
	
	static uint8_t LED_ALL_ON[] = "LED ALL ON";
	static uint8_t LED_1_ON[] = "LED 1 ON";
	static uint8_t LED_2_ON[] = "LED 2 ON";
	static uint8_t LED_3_ON[] = "LED 3 ON";
	static uint8_t LED_4_ON[] = "LED 4 ON";
	static uint8_t LED_5_ON[] = "LED 5 ON";
	static uint8_t LED_6_ON[] = "LED 6 ON";
	static uint8_t LED_7_ON[] = "LED 7 ON";
	static uint8_t LED_8_ON[] = "LED 8 ON";
	
	static uint8_t LED_ON_CONF[] = "LED Turned on";
	
	
	static uint8_t LED_ALL_OFF[] = "LED ALL OFF";
	static uint8_t LED_1_OFF[] = "LED 1 OFF";
	static uint8_t LED_2_OFF[] = "LED 2 OFF";
	static uint8_t LED_3_OFF[] = "LED 3 OFF";
	static uint8_t LED_4_OFF[] = "LED 4 OFF";
	static uint8_t LED_5_OFF[] = "LED 5 OFF";
	static uint8_t LED_6_OFF[] = "LED 6 OFF";
	static uint8_t LED_7_OFF[] = "LED 7 OFF";
	static uint8_t LED_8_OFF[] = "LED 8 OFF";
	
	static uint8_t LED_OFF_CONF[] = "LED Turned off";
	
	
	static uint8_t LED_ALL_STATE[] = "LED ALL STATE";
	static uint8_t LED_1_STATE[] = "LED 1 STATE";
	static uint8_t LED_2_STATE[] = "LED 2 STATE";
	static uint8_t LED_3_STATE[] = "LED 3 STATE";
	static uint8_t LED_4_STATE[] = "LED 4 STATE";
	static uint8_t LED_5_STATE[] = "LED 5 STATE";
  static uint8_t LED_6_STATE[] = "LED 6 STATE";
	static uint8_t LED_7_STATE[] = "LED 7 STATE";
	static uint8_t LED_8_STATE[] = "LED 8 STATE";

static uint8_t LED_STATE_ON[] = "LED is on";
static uint8_t LED_STATE_OFF[] = "LED is off";


CLI_Driver::CLI_Driver(): usart2(USART_Driver::_USART2)
{
	buffer = new uint8_t[BUF_MAX];
};

CLI_Driver::~CLI_Driver()
{
	if(buffer)
		delete[] buffer;
};

	void CLI_Driver::open(){
		// open the usart2 device
		usart2.open();
		//open the LED_Driver
		LED_Driver::open();
	}
	void CLI_Driver::close(){
		usart2.close();
		LED_Driver::close();
	}
	void CLI_Driver::start(){
		usart2.start();
		LED_Driver::start();
	}
	
	void CLI_Driver::stop(){
		usart2.stop();
		LED_Driver::stop();
	}	
	bool CLI_Driver::CLI()
	{
		newLine();
		while(1)
		{
			sendMenu();
			getLine();
			newLine();
			InterperetBuffer();
		}
		return true;
	}		

	

void CLI_Driver::getLine()
{
	newLine();
	bufferClear();
	printDoubleCarrot();
	int i = 0;
	uint8_t in_char;
	bool overflow = false;
	uint8_t overflow_message[] = "Invalid Command";
	while((in_char != ENTER))
	{
		// get a character while less than buffer size
		// Let the user type over the buffer and ignore excess characters

		in_char = usart2.GetByte();
		// if the user is within 30 characters
		if(i < BUF_MAX -1)
		{
			overflow = false;
			if(in_char != ENTER)
			{
				// delete last character
				if(in_char == BKSP && i > 0 )
				{
					i--;
					buffer[i] = 0x0;
				}
				else
				{
					buffer[i] = in_char;
					i++;
				}

			}
		}
		// if the user is over 30 characters
		else if(i >= BUF_MAX -1)
		{
			overflow = true;
			// if the character isn't a backspace
			// increment the buffer index
			if(in_char != BKSP)
				i++;
			else
			// if the character is a backspace, decrement it.
				i--;
			
		}
		if(i >= 0)
		//ghost inputs back to screen
		usart2.SendByte(in_char);
		else
			usart2.SendByte('>');
	}
	if(!overflow)
	{
		i++;
		buffer[i] = '\0';
	}
	else
	{
		sendLine(overflow_message);
		buffer[BUF_MAX - 1] = '\0';
	}
	
}

void CLI_Driver::sendLine(uint8_t* output)
{
	newLine();
	uint32_t i = 0;
	while(output[i] != '\0')
	{
		usart2.SendByte(output[i]);
		i++;
	}
}

void CLI_Driver::newLine()
{
	usart2.SendByte(0xA);
	usart2.SendByte(0xD);
}

void CLI_Driver::sendMenu()
{
	newLine();
	uint8_t Menu_1[] = "Enter a Command below. Use HELP to see a list of commands";
	sendLine(Menu_1);

}

void CLI_Driver::InterperetBuffer()
{
	bool state = false;
	if(bufferComp(HELP))
			sendHelp();
	else if (bufferComp(COMPILE_TIME))
	{
			sendLine(COMPILE_LINE);
	}
	else if (bufferComp(LED_ALL_ON))
	{
			LED_Driver::SetLED(LED_Driver::LED_ALL);
			sendLine(LED_ON_CONF);
	}
	else if (bufferComp(LED_1_ON))
	{
			LED_Driver::SetLED(LED_Driver::LED_1);
			sendLine(LED_ON_CONF);
	}
	else if (bufferComp(LED_2_ON))
	{
			LED_Driver::SetLED(LED_Driver::LED_2);
			sendLine(LED_ON_CONF);
	}
	else if (bufferComp(LED_3_ON))
	{
			LED_Driver::SetLED(LED_Driver::LED_3);
			sendLine(LED_ON_CONF);
	}
	else if (bufferComp(LED_4_ON))
	{
			LED_Driver::SetLED(LED_Driver::LED_4);
			sendLine(LED_ON_CONF);
	}
	else if (bufferComp(LED_5_ON))
	{
			LED_Driver::SetLED(LED_Driver::LED_5);
			sendLine(LED_ON_CONF);
	}
	else if (bufferComp(LED_6_ON))
	{
			LED_Driver::SetLED(LED_Driver::LED_6);
			sendLine(LED_ON_CONF);
	}
	else if (bufferComp(LED_7_ON))
	{
			LED_Driver::SetLED(LED_Driver::LED_7);
			sendLine(LED_ON_CONF);
	}
	else if (bufferComp(LED_8_ON))
	{
			LED_Driver::SetLED(LED_Driver::LED_8);
			sendLine(LED_ON_CONF);
	}
	
	else if (bufferComp(LED_ALL_OFF))
	{
			LED_Driver::ResetLED(LED_Driver::LED_ALL);
			sendLine(LED_OFF_CONF);
	}
	else if (bufferComp(LED_1_OFF))
	{
			LED_Driver::ResetLED(LED_Driver::LED_1);
			sendLine(LED_OFF_CONF);
	}
	else if (bufferComp(LED_2_OFF))
	{
			LED_Driver::ResetLED(LED_Driver::LED_2);
			sendLine(LED_OFF_CONF);
	}
	else if (bufferComp(LED_3_OFF))
	{
			LED_Driver::ResetLED(LED_Driver::LED_3);
			sendLine(LED_OFF_CONF);
	}
	else if (bufferComp(LED_4_OFF))
	{
			LED_Driver::ResetLED(LED_Driver::LED_4);
			sendLine(LED_OFF_CONF);
	}
	else if (bufferComp(LED_5_OFF))
	{
			LED_Driver::ResetLED(LED_Driver::LED_5);
			sendLine(LED_OFF_CONF);
	}
	else if (bufferComp(LED_6_OFF))
	{
			LED_Driver::ResetLED(LED_Driver::LED_6);
			sendLine(LED_OFF_CONF);
	}
	else if (bufferComp(LED_7_OFF))
	{
			LED_Driver::ResetLED(LED_Driver::LED_7);
			sendLine(LED_OFF_CONF);
	}
	else if (bufferComp(LED_8_OFF))
	{
			LED_Driver::ResetLED(LED_Driver::LED_8);
			sendLine(LED_OFF_CONF);
	}
	
	
	else if (bufferComp(LED_ALL_STATE))
	{	}
	else if (bufferComp(LED_1_STATE))
	{
			state = LED_Driver::StateLED(LED_Driver::LED_1);
			if(state)
				sendLine(LED_STATE_ON);
			else
				sendLine(LED_STATE_OFF);
	}
	else if (bufferComp(LED_2_STATE))
	{
			state = LED_Driver::StateLED(LED_Driver::LED_2);
			if(state)
				sendLine(LED_STATE_ON);
			else
				sendLine(LED_STATE_OFF);
	}
	else if (bufferComp(LED_3_STATE))
	{
			state = LED_Driver::StateLED(LED_Driver::LED_3);
			if(state)
				sendLine(LED_STATE_ON);
			else
				sendLine(LED_STATE_OFF);
	}
	else if (bufferComp(LED_4_STATE))
	{
			state = LED_Driver::StateLED(LED_Driver::LED_4);
			if(state)
				sendLine(LED_STATE_ON);
			else
				sendLine(LED_STATE_OFF);
	}
	else if (bufferComp(LED_5_STATE))
	{
			state = LED_Driver::StateLED(LED_Driver::LED_5);
			if(state)
				sendLine(LED_STATE_ON);
			else
				sendLine(LED_STATE_OFF);
	}
	else if (bufferComp(LED_6_STATE))
	{
			state = LED_Driver::StateLED(LED_Driver::LED_6);
			if(state)
				sendLine(LED_STATE_ON);
			else
				sendLine(LED_STATE_OFF);
	}
	else if (bufferComp(LED_7_STATE))
	{
			state = LED_Driver::StateLED(LED_Driver::LED_7);
			if(state)
				sendLine(LED_STATE_ON);
			else
				sendLine(LED_STATE_OFF);
	}
	else if (bufferComp(LED_8_STATE))
	{
			state = LED_Driver::StateLED(LED_Driver::LED_8);
			if(state)
				sendLine(LED_STATE_ON);
			else
				sendLine(LED_STATE_OFF);
	}
	
								
															
}

void CLI_Driver::printDoubleCarrot()
{
	usart2.SendByte('>');
	usart2.SendByte('>');
}

void CLI_Driver::bufferClear()
{
	// set the buffer to 0;
	for( int i = 0; i < BUF_MAX; i++)
		buffer[i] = 0x0;
}

bool CLI_Driver::bufferComp(uint8_t* command)
{
	int32_t i = 0;
	// escapes when one of the two strings reads an end of string character
	while((buffer[i] != '\0') && (command[i] != '\0'))
	{
		if(buffer[i] != command[i])
			return false;
		i++;
	}
	// if both strings are on an end of string character
	if((buffer[i] == '\0') && (command[i] == '\0'))
	return true;
	
	// else return false
	else return false;
}

void CLI_Driver::sendHelp()
{
	sendLine(HELP_LINE);
	sendLine(LED_LINE);
	sendLine(LED_ON_DESC_1);
	sendLine(LED_ON_DESC_2);
	sendLine(LED_OFF_DESC_1);
	sendLine(LED_OFF_DESC_2);
	sendLine(LED_STATE_DESC_1);
	sendLine(LED_STATE_DESC_2);
	sendLine(COMPILE_TIME);
	sendLine(COMPILE_TIME_DESC);
	sendLine(HELP);
	sendLine(HELP_DESC);
	
}
