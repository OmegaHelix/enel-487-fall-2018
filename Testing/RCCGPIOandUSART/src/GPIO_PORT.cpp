#include "GPIO_PORT.h"

GPIO_PORT::GPIO_PORT(uint32_t _BASE): _CRL (_BASE + 0x00),
																			_CRH (_BASE + 0x04),
																			_IDR (_BASE + 0x08),
																			_ODR (_BASE + 0x0C),
																			_BSRR(_BASE + 0x10),
																			_BRR (_BASE + 0x14),
																			_LCKR(_BASE + 0x18){};
GPIO_PORT::~GPIO_PORT(){};

	
void GPIO_PORT::Or(enum GPIO_PORT::PORT_ reg, uint32_t value)
{
	switch (reg)
	{
		case CRL:
			_CRL.Or(value);
			break;
		case CRH:
			_CRH.Or(value);
			break;
		case IDR:
			_IDR.Or(value);
			break;
		case ODR:
			_ODR.Or(value);
			break;
		case BSRR: 
			_BSRR.Or(value);
			break;
		case BRR: 
			_BRR.Or(value);
			break;
		case LCKR: 
			_LCKR.Or(value);
			break;
	};
};
void GPIO_PORT::And(enum GPIO_PORT::PORT_ reg, uint32_t value)
{
	switch (reg)
	{
		case CRL:
			_CRL.And(value);
			break;
		case CRH:
			_CRH.And(value);
			break;
		case IDR:
			_IDR.And(value);
			break;
		case ODR:
			_ODR.And(value);
			break;
		case BSRR: 
			_BSRR.And(value);
			break;
		case BRR: 
			_BRR.And(value);
			break;
		case LCKR: 
			_LCKR.And(value);
			break;
	};
};

void GPIO_PORT::Set(enum GPIO_PORT::PORT_ reg, uint32_t value)
{
	switch (reg)
	{
		case CRL:
			_CRL.Set(value);
			break;
		case CRH:
			_CRH.Set(value);
			break;
		case IDR:
			_IDR.Set(value);
			break;
		case ODR:
			_ODR.Set(value);
			break;
		case BSRR: 
			_BSRR.Set(value);
			break;
		case BRR: 
			_BRR.Set(value);
			break;
		case LCKR: 
			_LCKR.Set(value);
			break;
	};
};
uint32_t GPIO_PORT::Read(enum GPIO_PORT::PORT_ reg)
{
	uint32_t value = 0;
	switch (reg)
	{
		case CRL:
			value = _CRL.Read();
			break;
		case CRH:
			value = _CRH.Read();
			break;
		case IDR:
			value = _IDR.Read();
			break;
		case ODR:
			value = _ODR.Read();
			break;
		case BSRR: 
			value = _BSRR.Read();
			break;
		case BRR: 
			value = _BRR.Read();
			break;
		case LCKR: 
			value = _LCKR.Read();
			break;
	};
	return value;
};
