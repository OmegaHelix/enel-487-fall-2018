/**
 * Complex.cpp
 * 
 * Author: Dakota Fisher
 * 
 * Date: 2019-09-16
 * 
 * Function Definitions for the Complex class
 * 
 */





#include "Complex.h"
Complex::Complex()
{
          this->rl = 0;
          this->img = 0;
          this->NaN = false;
}

Complex::Complex(double rl, double img)
{       
        this->rl = rl;
        this->img = img;
        this->NaN = false;
}

// returns the result of lft_cmplx - rght_cmplx
 void Complex::subtract(Complex lft_cmplx, Complex rght_cmplx, Complex* result)
{
        result->sRl(lft_cmplx.gRl() - rght_cmplx.gRl()); 
        result->sImg(lft_cmplx.gImg() - rght_cmplx.gImg()); 
}

// returns the result of lft_cmplx + rght_cmplx
 void Complex::add(Complex lft_cmplx, Complex rght_cmplx, Complex* result)
{
        result->sRl(lft_cmplx.gRl() + rght_cmplx.gRl()); 
        result->sImg(lft_cmplx.gImg() + rght_cmplx.gImg()); 
}

// returns the result of lft_cmplx * rght_cmplx
 void Complex::multiply(Complex lft_cmplx, Complex rght_cmplx, Complex* result)
{
        result->sRl((lft_cmplx.gRl() * rght_cmplx.gRl()) 
                        - (lft_cmplx.gImg() * rght_cmplx.gImg())); 
        result->sImg((lft_cmplx.gRl() * rght_cmplx.gImg()) 
                        + (lft_cmplx.gImg() * rght_cmplx.gRl())); 
}

// returns the result of lft_cmplx / rght_cmplx
 void Complex::divide(Complex lft_cmplx, Complex rght_cmplx, Complex* result)            
{       
        double denominator = (rght_cmplx.gRl() * rght_cmplx.gRl()) 
                        + (rght_cmplx.gImg() * rght_cmplx.gImg());
        if(denominator == 0.0)
                result->sNaN(true);
        Complex* inverse = new Complex(rght_cmplx.gRl(), -(rght_cmplx.gImg()));
        Complex* numerator = new Complex();
        multiply(lft_cmplx, *inverse, numerator);

        result->sRl(numerator->gRl()/denominator);
        result->sImg(numerator->gImg()/denominator);
        delete inverse;
        delete numerator;
}

double Complex::gRl(void)
{
        return rl;
}

double Complex::gImg(void)
{
        return img;
}

bool Complex::gNaN(void)
{
        return NaN;
}

void Complex::sRl(double real)
{
        rl = real;
}

void Complex::sImg(double imaginary)
{
        img = imaginary;
}


void Complex::sNaN(bool set)
{
        NaN = set;
}


Complex::~Complex()
{
}