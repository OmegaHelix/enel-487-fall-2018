/******************************************************************************
 * 
 * Complex Number Calculator
 *  
 * File: Main.cpp
 * 
 * Author: Dakota Fisher
 * SID: 200 344 336
 * Date: 2018-09-16
 * Course: ENEL 487
 * Professor: Karim Naqvi
 * 
 * 
 * Parameters: 0, 1, or 2
 * optional parameter 1: input file
 * optional parameter 2: output file
 * 0 parameters: keyboard input mode
 * 1 parameter:  text file input mode, writes to cout
 * 2 parameters: text file input mode, writes to output file
 * 
 * The purpose of the main file is to host a main program loop and 
 * interperet user inputs to manipulate them using the Complex class.
 * 
 *****************************************************************************/









#include <cstdlib>
#include <iostream>
#include <cstdint>
#include <string>
#include <fstream>
#include <sstream>
#include <cmath>
#include "Complex.h"


//namespace used to make code easier to read
using namespace std;


int main(int argc, char const *argv[])
{
        //Variable Declarations
        ifstream in_file;
        ofstream out_file;
        string input_file = "";
        string output_file = "";
        string line;
        bool keyboard = false;
        // If no parameters are called with the program
        // Keyboard mode check
        if(argc == 1)
        {
                keyboard = true;
        }
        // If file names are included in the command line
        else if(argc > 1)
        {
                //Try and open input file if included
                input_file = argv[1];
                in_file.open(input_file);
                if(!in_file.is_open())
                {
                        cerr << "Unable to open input file!\n";
                        return 1;
                }
                if(argc > 2)
                {
                        //Try and open Output File if included
                        output_file = argv[2];
			//clearing contents of file if it exists
                        out_file.open(output_file, ofstream::trunc);
			out_file.close();
			//opening the file to work with it
			out_file.open(output_file);
                        if(!out_file.is_open())
                        {
                                cerr << "Unable to open output file!\n";
                                return 1;
                        }
                }
                
        }
        // If the system was run in Script mode
        char op;
        double ar,ai,br,bi;
        if(keyboard == false)
        {
                // Initiatlizing the complex numbers
                // and result
                Complex res;
                while(getline(in_file, line))
                {
                        //print blank lines as long as the input is
                        //one  character. 
                        //sideeffect: if the input is one invalid character,
                        //also prints a blank line.
                        while(line.length() == 1 && 
                                (line[0] != 'q' && line[0] != 'Q'))
                        {
                                if(out_file.is_open())
                                        out_file << endl;
                                else
                                        cout << endl;
                                getline(in_file, line);
                                
                        }
                        istringstream string_stream(line);
                        // Expected input format is
                        //operation real imaginary real imaginary
                        //exit condition for quitting
                        
                        
                        if(line[0] == 'q' || line[0] == 'Q')
                                return 0;
                        string_stream >> op >> ar >> ai >> br >> bi;
                        res.sNaN(false);
                        Complex B(br,bi);
                        Complex A(ar,ai);
                        switch(op)
                        {
                                case 'a':
                                case 'A':
                                        Complex::add(A,B,&res);
                                        break;
                                case 's':
                                case 'S':
                                        Complex::subtract(A,B,&res);
                                        break;
                                case 'd':
                                case 'D':
                                        Complex::divide(A,B,&res);
                                        break;
                                case 'm':
                                case 'M':
                                        Complex::multiply(A,B,&res);
                                        break;
                        }

                        if (out_file.is_open())
                        {
                                if(res.gNaN() == true)
                                {
                                        out_file << "NaN" << " + j ";
                                        out_file << "NaN" << endl; 
                                }
                                // if imaginary number is positive
                                else if(res.gImg() >= 0)
                                {
                                        out_file << res.gRl(); 
                                        out_file << " + j ";
                                        out_file << res.gImg() << endl;
                                }
                                //if the imaginary number is negative
                                else
                                {
                                        out_file << res.gRl();
                                        out_file << " - j ";
                                        out_file << abs(res.gImg()) << endl;
                                }
                        }
                        else 
                        {
                                if(res.gNaN() == true)
                                {
                                        cout << "NaN" << " + j ";
                                        cout << "NaN" << endl;
                                }
                                else
                                {
                                        // if imaginary number is positive
                                        if(res.gImg() >= 0)
                                        {
                                                cout << res.gRl(); 
                                                cout << " + j ";
                                                cout << res.gImg() << endl;
                                        }
                                        //if the imaginary number is negative
                                        else
                                        {
                                                cout << res.gRl();
                                                cout << " - j ";
                                                cout << abs(res.gImg());
                                                cout << endl;
                                        }
                                }
                        }
                }
                return 0;
        }
        cerr << "Type a letter to specify";
        cerr << " the arithmetic operator (A, S, M, D)" << endl;
        cerr << "followed by 2 complex numbers ";
        cerr << "expressed as pairs of doubles." << endl;
        cerr << "Type Q to quit." << endl;
        bool loop = true;
        while(loop == true)
        {
                cerr << "Enter exp:";
                cin >> op;
                if(op == 'q' || op == 'Q')
                {
                        cerr << "qitting";
                        break;
                }
                cin  >> ar >> ai >> br >> bi;
	        Complex B(br,bi);
                Complex A(ar,ai);
                Complex res;
                res.sNaN(false);
                switch(op)
                        {
                                case 'a':
                                case 'A':
                                        Complex::add(A,B,&res);
                                        break;
                                case 's':
                                case 'S':
                                        Complex::subtract(A,B,&res);
                                        break;
                                case 'd':
                                case 'D':
                                        Complex::divide(A,B,&res);
                                        break;
                                case 'm':
                                case 'M':
                                        Complex::multiply(A,B,&res);
                                        break;
                                default:
                                        return 0;
                        }
                if(res.gNaN() == true)
                       cout << "NaN" << " + j " << "NaN" << endl; 
                else
                {
                        // if imaginary number is positive
                        if(res.gImg() >= 0)
                        {
                                cout << res.gRl(); 
                                cout << " + j " << res.gImg() << endl;
                        }
                        //if the imaginary number is negative
                        else
                        {
                                cout << res.gRl();
                                cout << " - j " << abs(res.gImg()) << endl;
                        }
                }
        }
        return 0;
}



