Complex Number Calculator
author: Dakota Fisher
Date: 2018-09-16

This calculator can calculate the division, multiplication, subtraction, and 
addition of two complex numbers.

The Complex class contains 4 operation functions for the four operations.
The class also contains setters and getters for the values, as a standard
for object oriented programming. Given division by zero, it returns
NaN + j NaN.

how to build program:

Clone the directory from bitbucket and run the command:

g++ -std=c++0x Complex.h Complex.cpp Main.cpp -Wall -o ComplexCalculator

the program can be accessed by running ./ComplexCalculator with 0, 1 or 2
inputs.

depending on the version of g++ used, the following may be correct.

g++ -std=c++11 Complex.h Complex.cpp Main.cpp -Wall -o ComplexCalculator


Error Conditions:
The program expects that the inputs are of the form:
<operation> <real A> <imaginary A> <real B> <imaginary B>
if the operation is q or Q, the program simply exits.
if the operation is illegal, the program exits to prevent misuse.
the illegal operation could be improved upon in the future.

If the result of division is NaN, the program sets a bool in the complex
number to know to output the NaN value instead of the garbage result of
the division.

if the input from a file is a blank line, it prints a blank line to maintain
the format of the input document. Side effect of the blank line, is that it
ignores any single character inputs, aside from a special case for Q, or q.
This side effect is only a part of the batch mode.

if the input is between 2, 3, or 4 numbers, the program will wait for a 5th
before exiting due to the first character being invalid.

Code Formatting:
Tabs should be replaced with 8 spaces, and lines should be <= 80 characters.
JavaDocs format wasn't used for this assignment, but will be implemented in
the future.
