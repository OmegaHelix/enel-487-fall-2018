/**
 * 
 * Complex.h
 * 
 * Author: Dakota Fisher
 * 
 * Date: 2019-09-16
 * 
 * This class is a container for Complex numbers with functions to allow
 * for addition, subtraction, multiplication, and division.
 * 
 * In the event of division by zero, it sets a NaN bool to flag the
 * improper result.
 * 
 * 
 * 
 * 
 * 
 */ 


class Complex
{
public:
        Complex();
        Complex(double, double);
        static void add(Complex, Complex, Complex*);
        static void subtract(Complex, Complex, Complex*);
        static void divide(Complex, Complex, Complex*);
        static void multiply(Complex, Complex, Complex*);
        void sRl(double);
        void sImg(double);
        double gRl(void);
        double gImg(void);
        bool gNaN(void);
        void sNaN(bool);
        ~Complex();

private:
        double rl;
        double img;
        bool NaN;
};