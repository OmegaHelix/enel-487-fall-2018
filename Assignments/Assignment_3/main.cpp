#include <iostream>
#include <time.h>
#include <cmath>
#include "sin.h"
using namespace std;



void run_loop();
int main ()
{
	struct timespec start, stop;
	float fDegree = -359.5;
	float fResult = 0;
	float fRad = 0;
	int iDegree = 0;
	int iResult = 0;
	double iElapsed = 0;
	double fElapsed = 0;
	cout << "decimal\t\t\tLUT output\t\tLUT Time\t\tLib output\t\t\tLib Time\n";	
	for(iDegree = -3595; iDegree <= 3595; iDegree += 5)
	{
		clock_gettime( CLOCK_REALTIME, &start);
		iResult = scaled_sin(iDegree);
		clock_gettime( CLOCK_REALTIME, &stop);
		cout << fDegree << "\t\t\t" <<iResult <<"\t\t\t" << stop.tv_nsec - start.tv_nsec << " ns\t\t\t";
		fRad = fDegree * M_PI/180;
		clock_gettime( CLOCK_REALTIME, &start);
		fResult = sin(fRad);
		clock_gettime( CLOCK_REALTIME, &stop);
		cout << fResult << "\t\t\t" << stop.tv_nsec - start.tv_nsec  << " ns" << endl;
		fDegree += .5;
	}
        run_loop();
        run_loop();
        run_loop();
        run_loop();
        run_loop();
        run_loop();




}

void run_loop()
{
	float fDegree = -359.5;
	float fResult = 0;
	float fRad = 0;
	int iDegree = 0;
	int iResult = 0;
	struct timespec start, stop;
	cout << "\t\t\tLUT Time\t\t\t\t\tLib Time\n";	
	clock_gettime( CLOCK_REALTIME, &start);
	for(iDegree = -3595; iDegree <= 3595; iDegree += 5)
	iResult = scaled_sin(iDegree);
	clock_gettime( CLOCK_REALTIME, &stop);
	cout << "total" <<"\t\t\t" << stop.tv_nsec - start.tv_nsec<< " ns\t\t";
	//calculate Radian equivalent of degree
	// outside timer because only testing function speed.
	// Cal
	clock_gettime( CLOCK_REALTIME, &start);
	for(fDegree = -359.5; fDegree <= 359.5; fDegree += 0.5)
	fResult = sin(fDegree*M_PI/180);
	clock_gettime( CLOCK_REALTIME, &stop);
	cout << "\t\t\t" << stop.tv_nsec - start.tv_nsec << " ns" << endl;

}




