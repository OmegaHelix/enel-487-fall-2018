#include "sin.h"

const int SIN_LOOKUP_TABLE[181] = {				    //Degrees
	0,873,17452,2618,3490,4362,5234,6105,6976,7846,8716,9585,   //0    to 5 
	10453,11320,12187,13053,13917,14781,15643,16505,17365,18224,//5.5  to 10 
	19081,19937,20791,21644,22495,23345,24192,25038,25882,26724,//10.5 to 15 
	27564,28402,29237,30071,30902,31730,32557,33381,34202,35021,//15.5 to 20 
	35837,36650,37461,38268,39073,39875,40674,41469,42262,43051,//20.5 to 25 
	43837,44620,45399,46175,46947,47716,48481,49242,50000,50754,//25.5 to 30 
	51504,52250,52992,53730,54464,55194,55919,56641,57358,58070,//30.5 to 35 
	58779,59482,60182,60876,61566,62251,62932,63608,64279,64945,//35.5 to 40 
	65606,66262,66913,67559,68200,68835,69466,70091,70711,71325,//40.5 to 45 
	71934,72537,73135,73728,74314,74896,75471,76041,76604,77162,//45.5 to 50 
	77715,78261,78801,79335,79864,80386,80902,81412,81915,82413,//50.5 to 55 
	82904,83389,83867,84339,84805,85264,85717,86163,86603,87036,//55.5 to 60 
	87462,87882,88295,88701,89101,89493,89879,90259,90631,90996,//60.5 to 65 
	91355,91706,92050,92388,92718,93042,93358,93667,93969,94264,//65.5 to 70 
	94552,94832,95106,95372,95630,95882,96126,96363,96593,96815,//70.5 to 75 
	97030,97237,97437,97630,97815,97992,98163,98325,98481,98629,//75.5 to 80 
	98769,98902,99027,99144,99255,99357,99452,99540,99619,99692,//80.5 to 85 
	99756,99813,99863,99905,99939,99966,99985,99996,100000};    //85.5 to 90 



/**
 * scaled_sin(int)
 * 
 * Expectations: degree is an integer that is equal to 10 * the floating point
 *               value of the degree that sin is being calculated for. 
 * 
 * promises: no floating arithmetic is ever used.
 * 
 * returns: the sin value scaled by 100,000 as an integer 
 * 
 * 
 */


int scaled_sin(int degree)
{
	bool sign = true; // tracking sign. false is negative, true is positive
	// optimize zero case because no logic should be done
	if (degree == 0) return SIN_LOOKUP_TABLE[0];

	if (degree < 0)	  
	{
		degree = -degree;
		sign = false;
	}
        // check for bounds and trim to a legal range
	// since sin repeats it's self, just take the remainder
	// of the check.
	if (degree > 3600) degree  = degree % 3600;

        // Now compute logic to figure out which quadrant to correlate to
        // when interpereting the lookup table.
	if (degree > 1800)
	{
		degree -= 1800;
		sign = !sign;
	}
	if (degree >= 900) degree = 1800 - degree;
	// degree is now in the range of 0 to 900;
	// divide by 5 to get an index related to the lookup table
	degree = (degree / 5);
        //return the value of the lookup table with the correct sign.
        //sign is appended at the end, as not all quadrants have the same sign.
	return sign ? SIN_LOOKUP_TABLE[degree] : - SIN_LOOKUP_TABLE[degree];
}
