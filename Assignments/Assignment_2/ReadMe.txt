Random Password Generator (Snoopy Version)
author: Dakota Fisher
Date: 2018-10-01

This password generator simply randomly generates 5 numbers from a range of 
1 to 72877 and pulls the word from the row before that, treating the text
file "WordBank.txt" similar to a row-indexed linked list.

This simple program contains a XorShift128 function, and a main function.

XorShift128 is from Wikepedia, and provides a sufficiently random
password given the deterministic nature of linear programs.
Source: https://en.wikipedia.org/wiki/Xorshift

Main is a simple loop that grabs 5 random index values using XorShift128
and then prints out the words at row index-1 from the WordBank.txt file.

how to build program:

Clone the directory from bitbucket and run the command:

g++ -std=c++0x Main.cpp -Wall -o PasswordGenerator

depending on the version of g++ used, the following may be correct.

g++ -std=c++11 Main.cpp -Wall -o PasswordGenerator

Error Conditions:
        The program expects WordBank.txt to be located in the same
        directory as Main.cpp. Will error close if it is not. 

Outputs Possibilites:
        1. 5 Words from the WordBank.txt file.
        2. Error message if WordBank.txt cannot be opened.

Code Formatting:
        Tabs should be replaced with 8 spaces and lines should
        be <= 80 characters. Due to the simplicity of the program
        Javadocs formatting was not used. In a more complex system
        it would be worthwhile. 

Size ./PasswordGenerator Results:
   text    data     bss     dec     hex filename
   6229     792     584    7605    1db5 ./PasswordGenerator


