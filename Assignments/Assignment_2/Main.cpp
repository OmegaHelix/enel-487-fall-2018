/******************************************************************************
 * 
 * Random Password Generator
 * Assignment: 2
 * File: Main.cpp
 * Author: Dakota Fisher
 * SID: 200 344 336
 * Date: 2018-10-01
 * Course: ENEL 487
 * Professor: Karim Naqvi
 * 
 * Parameters: 0
 *      No parameters are taken into the program.
 * Requires:
 *      Companion File 'WordBank.txt' be in the same directory as Main.cpp
 * 
 *      The purpose of this Main.cpp file is to randomly generate 5 numbers 
 * corresponding to line numbers in a text file. The words at the specified
 * line number are then output to the screen. Creating a string of 5 words
 * to be used by the user as as strong, easily remembered password.
 * 
 *****************************************************************************/

#include <string>
#include <iostream>
#include <fstream>
#include <cstdint>

// namespace used to reduce line length
using namespace std;

// State variables for PRNG
static uint32_t x = time(NULL) + 12345678;
static uint32_t y = time(NULL) + 36912159;
static uint32_t w = time(NULL) + 24681012;
static uint32_t z = time(NULL) + 13579119;

/**
 * Pseudo-random value generator more suitable for
 * use on an embedded system, removes need for <random> library
 * 
 * Modified from: https://en.wikipedia.org/wiki/Xorshift
 * changes: 
 *      state[static 4] changed to 4 global variables.
 *      Attempts to use the state declaration with time()
 *      resulted in typecasting errors. 
 */
uint32_t xorshift128(void) {
    uint32_t t = x ^ (x << 11);
    x = y; y = z; z = w;
    return w = w ^ (w >> 19) ^ t ^ (t >> 8);
}


int32_t main ()
{
        // Variable declarations
        string word = "";
        string password = "";
        ifstream in_file;  
        // generate 5 random numbers from 0 to 72876
        // % 72877 generates a value from 1-72877. Loop uses <, so indexing is fine.
        uint32_t words[5] = {(xorshift128() % 72877), 
                             (xorshift128() % 72877),
                             (xorshift128() % 72877),
                             (xorshift128() % 72877),
                             (xorshift128() % 72877),};
        
        // Open WordBank file to pick Password words from
        in_file.open("WordBank.txt");
        if(!in_file.is_open())
        {
                cerr << "Unable to open WordBank file!\n";
                return 1;
        }
        // Grab the 5 words to print to the screen.
         for(uint32_t i = 0; i < 5; i++)
                {
                        // need to reset to line index 0 to grab each value
                        in_file.seekg(0);
                        // need to traverse to desired line to get word
                        // traverse to line index based on RNG value
                        for(uint32_t j = 0; j < words[i] ; j++)
                                getline(in_file, word);
                        // once the desired word is grabbed
                        // add it to the password string
                        password+=(word.substr(0, word.length()-1) + " ");
                }
                // output the password string
                cout << password << endl;
                //successful completion
                return 0;
}